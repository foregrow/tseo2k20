package tseo.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TseoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TseoApplication.class, args);
	}

}
