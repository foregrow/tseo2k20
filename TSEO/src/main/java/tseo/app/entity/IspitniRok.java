package tseo.app.entity;


import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import tseo.app.enums.NazivRoka;

@Entity
public class IspitniRok {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private NazivRoka nazivRoka;
	private Date pocetakRoka;
	private Date krajRoka;
	
	@OneToMany(mappedBy = "ispitniRok", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Set<Ispit> ispiti = new HashSet<Ispit>();
	
	public IspitniRok() {
		
	}

	public IspitniRok(long id, NazivRoka nazivRoka, Date pocetakRoka, Date krajRoka, Set<Ispit> ispiti) {
		super();
		this.id = id;
		this.nazivRoka = nazivRoka;
		this.pocetakRoka = pocetakRoka;
		this.krajRoka = krajRoka;
		this.ispiti = ispiti;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public NazivRoka getNazivRoka() {
		return nazivRoka;
	}

	public void setNazivRoka(NazivRoka nazivRoka) {
		this.nazivRoka = nazivRoka;
	}

	public Date getPocetakRoka() {
		return pocetakRoka;
	}

	public void setPocetakRoka(Date pocetakRoka) {
		this.pocetakRoka = pocetakRoka;
	}

	public Date getKrajRoka() {
		return krajRoka;
	}

	public void setKrajRoka(Date krajRoka) {
		this.krajRoka = krajRoka;
	}

	public Set<Ispit> getIspiti() {
		return ispiti;
	}

	public void setIspiti(Set<Ispit> ispiti) {
		this.ispiti = ispiti;
	}
	
	
}
