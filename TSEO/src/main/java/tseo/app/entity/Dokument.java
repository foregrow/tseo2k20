package tseo.app.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Dokument {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String naziv;
	private String tip;
	private String path;
	private byte[] dokument;
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	private Ucenik ucenik;
	
	
	public Dokument() {
		
	}


	public Dokument(long id, String naziv, String tip, String path, Ucenik ucenik, byte[] dokument) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.tip = tip;
		this.path = path;
		this.ucenik = ucenik;
		this.dokument = dokument;
	}


	public byte[] getDokument() {
		return dokument;
	}


	public void setDokument(byte[] dokument) {
		this.dokument = dokument;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getNaziv() {
		return naziv;
	}


	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}


	public String getTip() {
		return tip;
	}


	public void setTip(String tip) {
		this.tip = tip;
	}


	public String getPath() {
		return path;
	}


	public void setPath(String path) {
		this.path = path;
	}


	public Ucenik getUcenik() {
		return ucenik;
	}


	public void setUcenik(Ucenik ucenik) {
		this.ucenik = ucenik;
	}
	
	
}
