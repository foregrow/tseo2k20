package tseo.app.entity;

import java.util.HashSet;



import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import tseo.app.enums.Pol;

@Entity
public class Ucenik {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String ime;
	private String prezime;
	@Column(name="indexx", unique=true, nullable=false) 
	private String index;
	private int godinaUpisa;
	private int godinaStudija;
	
	//dodato
	private String drzavaRodjenja;
	private String mestoRodjenja;
	private String datumRodjenja;
	private Pol pol;
	private String nacinFinansiranja;
	private String email;
	private String adresa;
	private int ukupnoECTSBodova;
	private double prosecnaOcena;
	private String redniBrojUpisa;
	//
	
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	private Smer smer;
	
	@OneToOne(mappedBy = "ucenik")
	private FinansijskaKartica kartica;
	
	@OneToOne(mappedBy = "ucenik")
	private Korisnik korisnik;
	
	@OneToMany(mappedBy = "ucenik")
	private Set<Dokument> dokumenti = new HashSet<Dokument>();
	
	@OneToMany(mappedBy = "ucenik")
	private Set<SpisakUplata> uplate = new HashSet<SpisakUplata>();
	
	@ManyToMany(mappedBy = "ucenici")
	private Set<Predmet> predmeti = new HashSet<Predmet>();
	
	@OneToMany(mappedBy = "ucenik")
	private Set<Ispit> ispiti = new HashSet<Ispit>();
	
	@OneToMany(mappedBy = "ucenik", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Set<Kolokvijum> kolokvijumi = new HashSet<Kolokvijum>();
	
	public Ucenik() {
		
	}
	
	

	public Ucenik(long id, String ime, String prezime, String index, int godinaUpisa, int godinaStudija,
			String drzavaRodjenja, String mestoRodjenja, String datumRodjenja, Pol pol, String nacinFinansiranja,
			String email, String adresa, int ukupnoECTSBodova, double prosecnaOcena, Smer smer,
			FinansijskaKartica kartica, Korisnik korisnik, Set<Dokument> dokumenti, Set<Predmet> predmeti,
			Set<Ispit> ispiti,String redniBrojUpisa, Set<Kolokvijum> kolokvijumi, Set<SpisakUplata> uplate) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.index = index;
		this.godinaUpisa = godinaUpisa;
		this.godinaStudija = godinaStudija;
		this.drzavaRodjenja = drzavaRodjenja;
		this.mestoRodjenja = mestoRodjenja;
		this.datumRodjenja = datumRodjenja;
		this.pol = pol;
		this.nacinFinansiranja = nacinFinansiranja;
		this.email = email;
		this.adresa = adresa;
		this.ukupnoECTSBodova = ukupnoECTSBodova;
		this.prosecnaOcena = prosecnaOcena;
		this.smer = smer;
		this.kartica = kartica;
		this.korisnik = korisnik;
		this.dokumenti = dokumenti;
		this.predmeti = predmeti;
		this.ispiti = ispiti;
		this.redniBrojUpisa = redniBrojUpisa;
		this.kolokvijumi = kolokvijumi;
		this.uplate = uplate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public int getGodinaUpisa() {
		return godinaUpisa;
	}

	public void setGodinaUpisa(int godinaUpisa) {
		this.godinaUpisa = godinaUpisa;
	}

	public int getGodinaStudija() {
		return godinaStudija;
	}

	public void setGodinaStudija(int godinaStudija) {
		this.godinaStudija = godinaStudija;
	}

	public String getDrzavaRodjenja() {
		return drzavaRodjenja;
	}

	public void setDrzavaRodjenja(String drzavaRodjenja) {
		this.drzavaRodjenja = drzavaRodjenja;
	}

	public String getMestoRodjenja() {
		return mestoRodjenja;
	}

	public void setMestoRodjenja(String mestoRodjenja) {
		this.mestoRodjenja = mestoRodjenja;
	}

	public String getDatumRodjenja() {
		return datumRodjenja;
	}

	public void setDatumRodjenja(String datumRodjenja) {
		this.datumRodjenja = datumRodjenja;
	}

	public Pol getPol() {
		return pol;
	}

	public void setPol(Pol pol) {
		this.pol = pol;
	}

	public String getNacinFinansiranja() {
		return nacinFinansiranja;
	}

	public void setNacinFinansiranja(String nacinFinansiranja) {
		this.nacinFinansiranja = nacinFinansiranja;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public int getUkupnoECTSBodova() {
		return ukupnoECTSBodova;
	}

	public void setUkupnoECTSBodova(int ukupnoECTSBodova) {
		this.ukupnoECTSBodova = ukupnoECTSBodova;
	}

	public double getProsecnaOcena() {
		return prosecnaOcena;
	}

	public void setProsecnaOcena(double prosecnaOcena) {
		this.prosecnaOcena = prosecnaOcena;
	}

	public Smer getSmer() {
		return smer;
	}

	public void setSmer(Smer smer) {
		this.smer = smer;
	}

	public FinansijskaKartica getKartica() {
		return kartica;
	}

	public void setKartica(FinansijskaKartica kartica) {
		this.kartica = kartica;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public Set<Dokument> getDokumenti() {
		return dokumenti;
	}

	public void setDokumenti(Set<Dokument> dokumenti) {
		this.dokumenti = dokumenti;
	}

	public Set<Predmet> getPredmeti() {
		return predmeti;
	}

	public void setPredmeti(Set<Predmet> predmeti) {
		this.predmeti = predmeti;
	}

	public Set<Ispit> getIspiti() {
		return ispiti;
	}

	public void setIspiti(Set<Ispit> ispiti) {
		this.ispiti = ispiti;
	}

	public String getRedniBrojUpisa() {
		return redniBrojUpisa;
	}

	public void setRedniBrojUpisa(String redniBrojUpisa) {
		this.redniBrojUpisa = redniBrojUpisa;
	}

	public Set<Kolokvijum> getKolokvijumi() {
		return kolokvijumi;
	}

	public void setKolokvijumi(Set<Kolokvijum> kolokvijumi) {
		this.kolokvijumi = kolokvijumi;
	}



	public Set<SpisakUplata> getUplate() {
		return uplate;
	}



	public void setUplate(Set<SpisakUplata> uplate) {
		this.uplate = uplate;
	}
}
