package tseo.app.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class FinansijskaKartica {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String brojKartice;
	private double suma;
	//
	private String ziroRacun;
	private String pozivNaBroj;
	private String brojModela;
	//
	@OneToOne
	private Ucenik ucenik;
	
	public FinansijskaKartica() {
		
	}

	public FinansijskaKartica(long id, String brojKartice, double suma, String ziroRacun, String pozivNaBroj,
			String brojModela, Ucenik ucenik) {
		super();
		this.id = id;
		this.brojKartice = brojKartice;
		this.suma = suma;
		this.ziroRacun = ziroRacun;
		this.pozivNaBroj = pozivNaBroj;
		this.brojModela = brojModela;
		this.ucenik = ucenik;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getBrojKartice() {
		return brojKartice;
	}

	public void setBrojKartice(String brojKartice) {
		this.brojKartice = brojKartice;
	}

	public double getSuma() {
		return suma;
	}

	public void setSuma(double suma) {
		this.suma = suma;
	}

	public String getZiroRacun() {
		return ziroRacun;
	}

	public void setZiroRacun(String ziroRacun) {
		this.ziroRacun = ziroRacun;
	}

	public String getPozivNaBroj() {
		return pozivNaBroj;
	}

	public void setPozivNaBroj(String pozivNaBroj) {
		this.pozivNaBroj = pozivNaBroj;
	}

	public String getBrojModela() {
		return brojModela;
	}

	public void setBrojModela(String brojModela) {
		this.brojModela = brojModela;
	}

	public Ucenik getUcenik() {
		return ucenik;
	}

	public void setUcenik(Ucenik ucenik) {
		this.ucenik = ucenik;
	}

	
}
