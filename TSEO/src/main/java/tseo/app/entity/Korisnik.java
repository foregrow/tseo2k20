package tseo.app.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import tseo.app.enums.UlogaKorisnika;

@Entity
public class Korisnik {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String korisnickoIme;
	private String lozinka;
	private UlogaKorisnika uloga;
	
	@OneToOne
	private Ucenik ucenik;
	
	@OneToOne
	private Nastavnik nastavnik;
	
	
	
	public Korisnik() {
		
	}
	

	public Korisnik(long id, String korisnickoIme, String lozinka, UlogaKorisnika uloga, Ucenik ucenik,
			Nastavnik nastavnik) {
		super();
		this.id = id;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.uloga = uloga;
		this.ucenik = ucenik;
		this.nastavnik = nastavnik;
	}


	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getKorisnickoIme() {
		return korisnickoIme;
	}



	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}



	public String getLozinka() {
		return lozinka;
	}



	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}



	


	public UlogaKorisnika getUloga() {
		return uloga;
	}







	public void setUloga(UlogaKorisnika uloga) {
		this.uloga = uloga;
	}







	public Ucenik getUcenik() {
		return ucenik;
	}



	public void setUcenik(Ucenik ucenik) {
		this.ucenik = ucenik;
	}



	public Nastavnik getNastavnik() {
		return nastavnik;
	}



	public void setNastavnik(Nastavnik nastavnik) {
		this.nastavnik = nastavnik;
	}


	
	
}
