package tseo.app.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class SpisakUplata {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String svrha;
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	private Ucenik ucenik;
	private double suma;
	private boolean dodato;
	
	public SpisakUplata() {
		
	}
	
	public SpisakUplata(long id, String svrha, Ucenik ucenik, double suma,boolean dodato) {
		super();
		this.id = id;
		this.svrha = svrha;
		this.ucenik = ucenik;
		this.suma = suma;
		this.dodato = dodato;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getSvrha() {
		return svrha;
	}
	public void setSvrha(String svrha) {
		this.svrha = svrha;
	}
	public Ucenik getUcenik() {
		return ucenik;
	}
	public void setUcenik(Ucenik ucenik) {
		this.ucenik = ucenik;
	}
	public double getSuma() {
		return suma;
	}
	public void setSuma(double suma) {
		this.suma = suma;
	}

	public boolean isDodato() {
		return dodato;
	}

	public void setDodato(boolean dodato) {
		this.dodato = dodato;
	}
	
	
}
