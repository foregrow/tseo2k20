package tseo.app.entity;

import java.util.HashSet;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import tseo.app.enums.UlogaNastavnika;

@Entity
public class Nastavnik {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String ime;
	private String prezime;
	private String email;
	private UlogaNastavnika uloga;
	
	@OneToOne(mappedBy = "nastavnik")
	private Korisnik korisnik;
	
	@OneToOne(mappedBy = "nastavnik")
	private Smer smer;
	
	@ManyToMany(mappedBy = "nastavnici")
	private Set<Predmet> predmeti = new HashSet<Predmet>();
	
	
	public Nastavnik() {
		
	}
	


	public Nastavnik(long id, String ime, String prezime, String email, UlogaNastavnika uloga, Korisnik korisnik, Smer smer,
			Set<Predmet> predmeti) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.email = email;
		this.uloga = uloga;
		this.korisnik = korisnik;
		this.smer = smer;
		this.predmeti = predmeti;
	}



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public Set<Predmet> getPredmeti() {
		return predmeti;
	}

	public void setPredmeti(Set<Predmet> predmeti) {
		this.predmeti = predmeti;
	}

	public UlogaNastavnika getUloga() {
		return uloga;
	}

	public void setUloga(UlogaNastavnika uloga) {
		this.uloga = uloga;
	}



	public Smer getSmer() {
		return smer;
	}



	public void setSmer(Smer smer) {
		this.smer = smer;
	}
	
	
	
	
	
	
	
}
