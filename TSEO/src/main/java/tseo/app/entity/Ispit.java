package tseo.app.entity;



import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import tseo.app.enums.StatusIspita;

@Entity
public class Ispit {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String datumPolaganja;
	private String datumPrijave;
	private int ocena;
	private int bodoviTeorija;
	private int bodoviVezbe;
	private boolean polozen = false;
	private int cena;
	private StatusIspita status;
	
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	private Predmet predmet;
	
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	private Ucenik ucenik;
	
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	private IspitniRok ispitniRok;
	
	@OneToMany(mappedBy = "ispit", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Set<Kolokvijum> kolokvijumi = new HashSet<Kolokvijum>();
	
	public Ispit() {
		
	}

	public Ispit(long id, String datumPolaganja,String datumPrijave, int ocena, int bodoviTeorija, int bodoviVezbe, boolean polozen, int cena, StatusIspita status, Predmet predmet,
			Ucenik ucenik, IspitniRok ispitniRok, Set<Kolokvijum> kolokvijumi) {
		super();
		this.id = id;
		this.datumPolaganja = datumPolaganja;
		this.ocena = ocena;
		this.bodoviTeorija = bodoviTeorija;
		this.bodoviVezbe = bodoviVezbe;
		this.polozen = polozen;
		this.cena = cena;
		this.predmet = predmet;
		this.ucenik = ucenik;
		this.ispitniRok = ispitniRok;
		this.datumPrijave = datumPrijave;
		this.kolokvijumi = kolokvijumi;
		this.status = status;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDatumPolaganja() {
		return datumPolaganja;
	}

	public void setDatumPolaganja(String datumPolaganja) {
		this.datumPolaganja = datumPolaganja;
	}
	

	public String getDatumPrijave() {
		return datumPrijave;
	}

	public void setDatumPrijave(String datumPrijave) {
		this.datumPrijave = datumPrijave;
	}

	public int getOcena() {
		return ocena;
	}

	public void setOcena(int ocena) {
		this.ocena = ocena;
	}

	public int getBodoviTeorija() {
		return bodoviTeorija;
	}

	public void setBodoviTeorija(int bodoviTeorija) {
		this.bodoviTeorija = bodoviTeorija;
	}

	public int getBodoviVezbe() {
		return bodoviVezbe;
	}

	public void setBodoviVezbe(int bodoviVezbe) {
		this.bodoviVezbe = bodoviVezbe;
	}

	public boolean isPolozen() {
		return polozen;
	}

	public void setPolozen(boolean polozen) {
		this.polozen = polozen;
	}

	public int getCena() {
		return cena;
	}

	public void setCena(int cena) {
		this.cena = cena;
	}

	public Predmet getPredmet() {
		return predmet;
	}

	public void setPredmet(Predmet predmet) {
		this.predmet = predmet;
	}


	public Ucenik getUcenik() {
		return ucenik;
	}

	public void setUcenik(Ucenik ucenik) {
		this.ucenik = ucenik;
	}

	public IspitniRok getIspitniRok() {
		return ispitniRok;
	}

	public void setIspitniRok(IspitniRok ispitniRok) {
		this.ispitniRok = ispitniRok;
	}

	public Set<Kolokvijum> getKolokvijumi() {
		return kolokvijumi;
	}

	public void setKolokvijumi(Set<Kolokvijum> kolokvijumi) {
		this.kolokvijumi = kolokvijumi;
	}

	public StatusIspita getStatus() {
		return status;
	}

	public void setStatus(StatusIspita status) {
		this.status = status;
	}
	
}
