package tseo.app.entity;





import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Kolokvijum {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private float bodovi;
	private String datumPolaganja;
	private String naziv; 
	
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	private Predmet predmet;
	
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	private Ispit ispit;
	
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	private Ucenik ucenik;
	
	public Kolokvijum() {
		
	}
	
	public Kolokvijum(long id, float bodovi, String datumPolaganja, Predmet predmet, Ispit ispit,String naziv,
			Ucenik ucenik) {
		super();
		this.id = id;
		this.bodovi = bodovi;
		this.datumPolaganja = datumPolaganja;
		this.predmet = predmet;
		this.ispit = ispit;
		this.ucenik = ucenik;
		this.naziv = naziv;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public float getBodovi() {
		return bodovi;
	}

	public void setBodovi(float bodovi) {
		this.bodovi = bodovi;
	}

	public String getDatumPolaganja() {
		return datumPolaganja;
	}

	public void setDatumPolaganja(String datumPolaganja) {
		this.datumPolaganja = datumPolaganja;
	}

	public Predmet getPredmet() {
		return predmet;
	}

	public void setPredmet(Predmet predmet) {
		this.predmet = predmet;
	}

	public Ispit getIspit() {
		return ispit;
	}

	public void setIspit(Ispit ispit) {
		this.ispit = ispit;
	}

	public Ucenik getUcenik() {
		return ucenik;
	}

	public void setUcenik(Ucenik ucenik) {
		this.ucenik = ucenik;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
}
