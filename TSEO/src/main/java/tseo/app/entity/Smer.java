package tseo.app.entity;

import java.util.HashSet;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Smer {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String naziv;
	
	//dodato
	private int brojECTSBodova;
	private String oznakaSmera;
	//
	@OneToOne
	private Nastavnik nastavnik;
	
	@OneToMany(mappedBy = "smer")
	private Set<Ucenik> ucenici = new HashSet<Ucenik>();
	
	@OneToMany(mappedBy = "smer")
	private Set<Predmet> predmeti = new HashSet<Predmet>();
	
	public Smer() {
		
	}

	public Smer(long id, String naziv, Nastavnik nastavnik, Set<Ucenik> ucenici, Set<Predmet> predmeti, int brojECTSBodova, String oznakaSmera) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.brojECTSBodova = brojECTSBodova;
		this.nastavnik = nastavnik;
		this.ucenici = ucenici;
		this.predmeti = predmeti;
		this.oznakaSmera = oznakaSmera;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	

	public int getBrojECTSBodova() {
		return brojECTSBodova;
	}

	public void setBrojECTSBodova(int brojECTSBodova) {
		this.brojECTSBodova = brojECTSBodova;
	}

	public Nastavnik getNastavnik() {
		return nastavnik;
	}

	public void setNastavnik(Nastavnik nastavnik) {
		this.nastavnik = nastavnik;
	}

	public Set<Ucenik> getUcenici() {
		return ucenici;
	}

	public void setUcenici(Set<Ucenik> ucenici) {
		this.ucenici = ucenici;
	}

	public Set<Predmet> getPredmeti() {
		return predmeti;
	}

	public void setPredmeti(Set<Predmet> predmeti) {
		this.predmeti = predmeti;
	}

	public String getOznakaSmera() {
		return oznakaSmera;
	}

	public void setOznakaSmera(String oznakaSmera) {
		this.oznakaSmera = oznakaSmera;
	}
	
	
	
}
