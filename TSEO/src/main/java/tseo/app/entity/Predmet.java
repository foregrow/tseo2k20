package tseo.app.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Predmet {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String naziv;
	
	//dodato
	private int brojECTSBodova;
	private String datumPolaganja;
	//
	@ManyToMany
    @JoinTable(name = "pohadja",
               joinColumns = @JoinColumn(name="predmet_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="ucenik_id", referencedColumnName="id"))
	private Set<Ucenik> ucenici = new HashSet<Ucenik>();
	
	@ManyToMany
    @JoinTable(name = "predaje",
               joinColumns = @JoinColumn(name="predmet_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="nastavnik_id", referencedColumnName="id"))
	private Set<Nastavnik> nastavnici = new HashSet<Nastavnik>();
	
	@OneToMany(mappedBy = "predmet", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Set<Ispit> ispiti = new HashSet<Ispit>();
	
	@OneToMany(mappedBy = "predmet", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Set<Kolokvijum> kolokvijumi = new HashSet<Kolokvijum>();
	
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	private Smer smer;
	
	public Predmet() {
		
	}

	public Predmet(long id, String naziv, Set<Ucenik> ucenici, Set<Nastavnik> nastavnici,
			Set<Ispit> ispiti, Set<Kolokvijum> kolokvijumi, Smer smer,int brojECTSBodova,String datumPolaganja) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.ucenici = ucenici;
		this.nastavnici = nastavnici;
		this.ispiti = ispiti;
		this.kolokvijumi = kolokvijumi;
		this.smer = smer;
		this.brojECTSBodova = brojECTSBodova;
		this.datumPolaganja = datumPolaganja;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	
	public int getBrojECTSBodova() {
		return brojECTSBodova;
	}

	public void setBrojECTSBodova(int brojECTSBodova) {
		this.brojECTSBodova = brojECTSBodova;
	}

	public Set<Ucenik> getUcenici() {
		return ucenici;
	}

	public void setUcenici(Set<Ucenik> ucenici) {
		this.ucenici = ucenici;
	}

	public Set<Nastavnik> getNastavnici() {
		return nastavnici;
	}

	public void setNastavnici(Set<Nastavnik> nastavnici) {
		this.nastavnici = nastavnici;
	}

	public Set<Ispit> getIspiti() {
		return ispiti;
	}

	public void setIspiti(Set<Ispit> ispiti) {
		this.ispiti = ispiti;
	}

	public Set<Kolokvijum> getKolokvijumi() {
		return kolokvijumi;
	}

	public void setKolokvijumi(Set<Kolokvijum> kolokvijumi) {
		this.kolokvijumi = kolokvijumi;
	}

	public Smer getSmer() {
		return smer;
	}

	public void setSmer(Smer smer) {
		this.smer = smer;
	}

	public String getDatumPolaganja() {
		return datumPolaganja;
	}

	public void setDatumPolaganja(String datumPolaganja) {
		this.datumPolaganja = datumPolaganja;
	}
	
	
	
	
	
	
}
