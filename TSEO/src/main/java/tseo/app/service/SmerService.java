package tseo.app.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tseo.app.entity.Smer;
import tseo.app.repository.SmerRepository;

@Service
public class SmerService {

	@Autowired
	SmerRepository sr;
	
	public List<Smer> findAll() {
		return sr.findAll();
	}

	public Smer findOne(long id) {
		return sr.findById(id).orElse(null);
	}
	
	public Smer save(Smer uc) {
		return sr.save(uc);
	}

	public void remove(long id) {
		sr.deleteById(id);
	}
	
	public Smer findByNastavnikId(long id) {
		return sr.findByNastavnikId(id);
	}
	
	public Smer findByOznakaSmera(String oznakaSmera) {
		return sr.findByOznakaSmera(oznakaSmera);
	}
	
	public List<Smer> findSmeroviByPredmetNaziv(String predName){
		return sr.findSmeroviByPredmetNaziv(predName);
	}
	
}
