package tseo.app.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tseo.app.entity.Ispit;
import tseo.app.entity.Ucenik;
import tseo.app.repository.UcenikRepository;

@Service
public class UcenikService {
	
	@Autowired
	UcenikRepository ur;
	
	public List<Ucenik> findAll() {
		return ur.findAll();
	}

	public Ucenik findOne(long id) {
		return ur.findById(id).orElse(null);
	}
	
	public Ucenik save(Ucenik uc) {
		return ur.save(uc);
	}

	public void remove(long id) {
		ur.deleteById(id);
	}
	
	public List<Ucenik> getAllFromUcenik(){
		return ur.getAllFromUcenik();
	}
	
	public Ucenik getByIndex(String index) {
		return ur.getByIndex(index);
	}
	
	public int countUceniciBySmerAndGodinaUpisa(String str,int god) {
		return ur.countUceniciBySmerAndGodinaUpisa(str,god);
	}
	
	public List<String> getIndexesBySmerAndGodinaUpisa(String str,int god){
		return ur.getIndexesBySmerAndGodinaUpisa(str,god);
	}
	
	public String createIndex(String oznakaSmera,int godinaInt) {
		int countUcenici = ur.countUceniciBySmerAndGodinaUpisa(oznakaSmera, godinaInt);
		List<String> smerIndexi = ur.getIndexesBySmerAndGodinaUpisa(oznakaSmera, godinaInt);
		String newIndex = "";
		String oznakaSmeraUP = oznakaSmera.toUpperCase();
		String godinaUpisa = Integer.toString(godinaInt);
		if(countUcenici == 0) {
			//znaci da nema nijednog na tom smeru
			newIndex = oznakaSmeraUP+ "-1-" + godinaUpisa;
		}else {
			do {
				countUcenici++;
				String redniBroj = Integer.toString(countUcenici);
				newIndex = oznakaSmeraUP+ "-"+redniBroj+"-" + godinaUpisa;
			}while (smerIndexi.contains(newIndex));
			
		}
		return newIndex;
	}
	
	public double prosecnaOcena(int novaOcena,Ucenik uc) {
		int counter = 0;
		int ocena = 0;
		for(Ispit ispit : uc.getIspiti()) {
			if(ispit.getOcena() > 5) {
				ocena += ispit.getOcena();
				counter++;
			}		
		}
		double prosecna = ocena / counter;
			
		
		return prosecna;
	}

	

}
