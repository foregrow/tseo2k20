package tseo.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tseo.app.entity.Kolokvijum;
import tseo.app.repository.KolokvijumRepository;

@Service
public class KolokvijumService {

	
	@Autowired
	KolokvijumRepository kr;
	
	public List<Kolokvijum> findAll() {
		return kr.findAll();
	}

	public Kolokvijum findOne(long id) {
		return kr.findById(id).orElse(null);
	}
	
	public Kolokvijum save(Kolokvijum k) {
		return kr.save(k);
	}

	public void remove(long id) {
		kr.deleteById(id);
	}
	
	public int countBrojKlkova(long uid, long pid) {
		return kr.countBrojKlkova(uid, pid);
	}
}
