package tseo.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import tseo.app.entity.Dokument;
import tseo.app.entity.Ucenik;
import tseo.app.repository.DokumentRepository;

@Service
public class DokumentService {

	
	@Autowired
	DokumentRepository dr;
	
	public void save(Dokument dok) {
		dr.save(dok);
	}
	public Dokument saveFile(MultipartFile file, Ucenik u) {
		String docName = file.getOriginalFilename();
		try {
			Dokument doc = new Dokument();
			doc.setNaziv(docName);
			doc.setTip(file.getContentType());
			doc.setDokument(file.getBytes());
			doc.setUcenik(u);
			return dr.save(doc);
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public List<Dokument> getByUcenikId(long uid){
		return dr.findByUcenikId(uid);
	}
	public Optional<Dokument> getById(long id) {
		return dr.findById(id);
	}
	public List<Dokument> getAll(){
		return dr.findAll();
	}
	
	public void remove(long id) {
		dr.deleteById(id);
	}
}
