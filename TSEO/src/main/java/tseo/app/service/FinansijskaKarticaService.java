package tseo.app.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tseo.app.entity.FinansijskaKartica;
import tseo.app.repository.FinansijskaKarticaRepository;

@Service
public class FinansijskaKarticaService {

	
	@Autowired
	FinansijskaKarticaRepository fkr;
	
	public List<FinansijskaKartica> findAll(){
		return fkr.findAll();
	}
	
	public FinansijskaKartica save(FinansijskaKartica fk) {
		return fkr.save(fk);
	}
	
	public FinansijskaKartica findByUcenikId(long id) {
		return fkr.findByUcenikId(id);
	}
	
	public FinansijskaKartica findByBrojKartice(String brojKartice) {
		return fkr.findByBrojKartice(brojKartice);
	}
	
	public int countFinansijskeKartice() {
		return fkr.countFinansijskeKartice();
	}
	
	public String createBrojKartice() {
		int countKartice = fkr.countFinansijskeKartice();
		FinansijskaKartica fk = null;
		String newBrojKartice = "";
		final String brojKarticeConst = "00";
		if(countKartice == 0) {
			newBrojKartice = brojKarticeConst + "1";
		}else {
			do {
				countKartice++;
				String redniBroj = Integer.toString(countKartice);
				newBrojKartice = brojKarticeConst + redniBroj;
				fk = fkr.findByBrojKartice(newBrojKartice);
				//ako je nova fkr nece pronaci karticu i fk ostaje null
				//ako je postojeca fkr pronalazi karticu i fk nije null i ponovo se pokrece increment
			}while(fk != null);
			
			
		}	
		return newBrojKartice;
	}
}
