package tseo.app.service;

import java.util.ArrayList;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import tseo.app.entity.Korisnik;
import tseo.app.enums.UlogaKorisnika;
import tseo.app.enums.UlogaNastavnika;
import tseo.app.repository.KorisnikRepository;

@Service
public class KorisnikService implements UserDetailsService {

	@Autowired
	KorisnikRepository kr;
	
	public List<Korisnik> findAll() {
		return kr.findAll();
	}

	public Korisnik findOne(long id) {
		return kr.findById(id).orElse(null);
	}
	
	public Korisnik save(Korisnik kor) {
		return kr.save(kor);
	}

	public void remove(long id) {
		kr.deleteById(id);
	}
	

	public Korisnik findByKorisnickoIme(String username) {
		return kr.findByKorisnickoIme(username);
	}
	
	public Korisnik findByNastavnikId(long id) {
		return kr.findByNastavnikId(id);
	}
	
	public Korisnik findByNastavnikIdAndKorisnickoIme(long idTrazenogNastavnika,String korisnickoImeUlogovanog) {
		return kr.findByNastavnikIdAndKorisnickoIme(idTrazenogNastavnika, korisnickoImeUlogovanog);
	}
	
	public Korisnik findByUcenikIdAndKorisnickoIme(long idTrazenogUcenika,String korisnickoImeUlogovanog) {
		return kr.findByUcenikIdAndKorisnickoIme(idTrazenogUcenika, korisnickoImeUlogovanog);
	}
	
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Korisnik user = kr.findByKorisnickoIme(username);
		String uloga = "";
		if(user.getUloga() == UlogaKorisnika.ROLE_KORISNIK) {
			if(user.getUcenik() != null)
				uloga = "ROLE_UCENIK";
			else if(user.getNastavnik() != null) {
				if(user.getNastavnik().getUloga().equals(UlogaNastavnika.PROFESOR)) {
					uloga = "ROLE_NASTAVNIK";
				}else if(user.getNastavnik().getUloga().equals(UlogaNastavnika.ASISTENT)) {
					uloga = "ROLE_ASISTENT";
				}else if(user.getNastavnik().getUloga().equals(UlogaNastavnika.DEMONSTRATOR)) {
					uloga = "ROLE_DEMONSTRATOR";
				}
			}
		}else if(user.getUloga() == UlogaKorisnika.ROLE_ADMIN)
			uloga = "ROLE_ADMIN";
			
		List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
		grantedAuthorities.add(new SimpleGrantedAuthority(uloga));
		return new org.springframework.security.core.userdetails.User(user.getKorisnickoIme(),user.getLozinka(), grantedAuthorities);
	}
}
