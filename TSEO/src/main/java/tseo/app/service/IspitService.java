package tseo.app.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tseo.app.entity.Ispit;
import tseo.app.entity.Ucenik;
import tseo.app.enums.StatusIspita;
import tseo.app.repository.IspitRepository;

@Service
public class IspitService {

	
	@Autowired
	IspitRepository ir;
	
	public List<Ispit> findAll() {
		return ir.findAll();
	}

	public Ispit findOne(long id) {
		return ir.findById(id).orElse(null);
	}
	
	public Ispit save(Ispit i) {
		return ir.save(i);
	}

	public void remove(long id) {
		ir.deleteById(id);
	}
	
	public Ispit ispitZaOdjavu(long uid, long pid, long irid) {
		return ir.ispitZaOdjavu(uid, pid, irid);
	}
	
	public List<Ispit> findByIspitniRokId(long irid){
		return ir.findByIspitniRokId(irid);
	}
	
	public List<Ispit> findByUcenikIdAndStatus(long uid, StatusIspita status){
		return ir.findByUcenikIdAndStatus(uid, status);
	}
	
	public List<Ucenik> getUceniciPrijaviliIspit(long pid,long irid){
		return ir.getUceniciPrijaviliIspit(pid, irid);
	}
	
	public Ispit findByIspitniRokIdAndUcenikIdAndPredmetIdAndStatusAndPolozen(long irid, long uid, long pid, StatusIspita status, boolean polozen) {
		return ir.findByIspitniRokIdAndUcenikIdAndPredmetIdAndStatusAndPolozen(irid, uid, pid, status, polozen);
	}
	
	public int createOcena(int teorija, int vezbe) {
		int ocena = 5;
		int bodovi = teorija + vezbe;
		if(bodovi > 100)
			bodovi = 100;
		
		if(bodovi >= 91 && bodovi <= 100)
			ocena = 10;
		else if(bodovi >= 81 && bodovi < 91)
			ocena = 9;
		else if(bodovi >= 71 && bodovi < 81) 
			ocena = 8;
		else if(bodovi >= 61 && bodovi < 71)
			ocena = 7;
		else if(bodovi >= 51 && bodovi < 61)
			ocena = 6;
		else
			ocena = 5;
		
		return ocena;
	}
		
	public List<Ispit> findByPredmetIdAndIspitniRokId(long pid, long irid){
		return ir.findByPredmetIdAndIspitniRokId(pid, irid);
	}
}
