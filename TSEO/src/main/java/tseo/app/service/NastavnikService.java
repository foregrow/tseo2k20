package tseo.app.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tseo.app.entity.Nastavnik;
import tseo.app.repository.NastavnikRepository;

@Service
public class NastavnikService {
	
	@Autowired
	NastavnikRepository nr;
	
	public List<Nastavnik> findAll() {
		return nr.findAll();
	}

	public Nastavnik findOne(long id) {
		return nr.findById(id).orElse(null);
	}
	
	public Nastavnik save(Nastavnik n) {
		return nr.save(n);
	}

	public void remove(long id) {
		nr.deleteById(id);
	}
	
	public List<Nastavnik> getAllFromNastavnik(){
		return nr.getAllFromNastavnik();
	}
	
	public Nastavnik getByEmail(String email) {
		return nr.getByEmail(email);
	}
	
	public List<Nastavnik> getAllWhereSefKatedreNull(){
		return nr.getAllWhereSefKatedreNull();
	}
	
	public Nastavnik getNastavnikByPredmet(long predmetId) {
		return nr.getNastavnikByPredmet(predmetId);
	}
	
	public Nastavnik getByKorisnikKorisnickoIme(String korIme) {
		return nr.getByKorisnikKorisnickoIme(korIme);
	}
	

}
