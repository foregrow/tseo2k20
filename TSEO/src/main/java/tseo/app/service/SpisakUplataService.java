package tseo.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tseo.app.entity.SpisakUplata;
import tseo.app.repository.SpisakUplataRepository;

@Service
public class SpisakUplataService {

	@Autowired
	SpisakUplataRepository sur;
	
	public void save(SpisakUplata su) {
		sur.save(su);
	}
	
	public List<SpisakUplata> findByUcenikId(long uid){
		return sur.findByUcenikId(uid);
	}
}
