package tseo.app.service;


import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import tseo.app.entity.Predmet;
import tseo.app.repository.PredmetRepository;

@Service
public class PredmetService {

	@Autowired
	PredmetRepository pr;

	public List<Predmet> findAll() {
		return pr.findAll();
	}

	public Predmet findOne(long id) {
		return pr.findById(id).orElse(null);
	}
	
	public Predmet save(Predmet predmet) {
		return pr.save(predmet);
	}

	public void remove(long id) {
		pr.deleteById(id);
	}
	
	public List<Predmet> getPredmetiNastavnikPredaje(long id){
		return pr.getPredmetiNastavnikPredaje(id);
	}
	
	public List<Predmet> getPredmetiNastavnikNePredaje(long id){
		return pr.getPredmetiNastavnikNePredaje(id);
	}
	
	public Predmet findByNaziv(String naziv) {
		return pr.findByNaziv(naziv);
	}
	
	public List<Predmet> getPredmetiUcenikPohadja(long id){
		return pr.getPredmetiUcenikPohadja(id);
	}
	
	public List<Predmet> getPredmetiNastavniciPredaju(){
		return pr.getPredmetiNastavniciPredaju();
	}
	
	public List<Predmet> getNepolozeniPredmeti(long smerId, long ucenikId){
		return pr.getNepolozeniPredmeti(smerId, ucenikId);
	}
	
	public List<Predmet> getPredmetiZaPrijavu(long smerId, long ucenikId, long irok){
		return pr.getPredmetiZaPrijavu(smerId, ucenikId, irok);
	}
	
	public List<Predmet> getPrijavljeniPredmetiZaIspit(long ucenikId,long iRok){
		return pr.getPrijavljeniPredmetiZaIspit(ucenikId, iRok);
	}

}
