package tseo.app.service;


import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tseo.app.entity.IspitniRok;
import tseo.app.repository.IspitniRokRepository;

@Service
public class IspitniRokService {

	
	@Autowired
	IspitniRokRepository irr;
	
	
	public List<IspitniRok> getAll(){
		return irr.findAll();
	}
	
	public void save(IspitniRok irok){
		irr.save(irok);
	}
	
	public IspitniRok getTrenutniRok(Date datum) {
		return irr.getTrenutniRok(datum);
		
	}
	
	public IspitniRok getIspitniRokById(long id) {
		return irr.findById(id).orElse(null);
	}
}
