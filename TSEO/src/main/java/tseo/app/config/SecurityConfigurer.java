package tseo.app.config;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import tseo.app.filter.JwtRequestFilter;
import tseo.app.service.KorisnikService;

@EnableWebSecurity
public class SecurityConfigurer extends WebSecurityConfigurerAdapter{
	@Autowired
	private KorisnikService UserService;
	
	@Autowired
	private JwtRequestFilter jwtRequestFilter;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		auth.userDetailsService(UserService);
		
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
			.sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and()
			.authorizeRequests()
				.antMatchers("/authenticate").permitAll()
				.antMatchers("/api/finansijskaKartica").hasAnyRole("ADMIN","UCENIK")
				.antMatchers("/api/finansijskaKartica/**").hasAnyRole("ADMIN","UCENIK")
				.antMatchers(HttpMethod.POST,"/api/ispiti").hasAnyRole("ADMIN","UCENIK")
				.antMatchers(HttpMethod.POST,"/api/ispiti/**").hasAnyRole("ADMIN","UCENIK")
				.antMatchers(HttpMethod.PUT,"/api/ispiti/posledjivanjeOcene").hasRole("NASTAVNIK")
				.antMatchers(HttpMethod.PUT,"/api/ispiti/addDatumPolaganja").hasRole("ADMIN") 
				.antMatchers(HttpMethod.PUT,"/api/ispit/odjavaIspita").hasAnyRole("UCENIK") 
				.antMatchers(HttpMethod.GET,"/api/ispit/uceniciPrijaviliIspit").hasAnyRole("NASTAVNIK","ADMIN") 
				.antMatchers(HttpMethod.PUT,"/api/ispitniRok").hasRole("ADMIN") 
				.antMatchers(HttpMethod.POST,"/api/kolokvijumi").hasRole("ASISTENT")
				.antMatchers(HttpMethod.POST,"/api/korisnici").hasRole("ADMIN")
				.antMatchers(HttpMethod.DELETE,"/api/korisnici").hasRole("ADMIN")
				.antMatchers(HttpMethod.POST,"/api/nastavnici").hasRole("ADMIN")
				.antMatchers(HttpMethod.DELETE,"/api/nastavnici").hasRole("ADMIN")
				.antMatchers(HttpMethod.PUT,"/api/nastavnici/addPredmeteNastavniku").hasRole("ADMIN") 
				.antMatchers(HttpMethod.POST,"/api/predmeti").hasRole("ADMIN")
				.antMatchers(HttpMethod.PUT,"/api/predmeti").hasRole("ADMIN")
				.antMatchers(HttpMethod.DELETE,"/api/predmeti").hasRole("ADMIN")
				.antMatchers(HttpMethod.POST,"/api/smerovi").hasRole("ADMIN")
				.antMatchers(HttpMethod.PUT,"/api/smerovi").hasRole("ADMIN")
				.antMatchers(HttpMethod.DELETE,"/api/smerovi").hasRole("ADMIN")
				.antMatchers(HttpMethod.POST,"/api/ucenici").hasRole("ADMIN")
				.antMatchers(HttpMethod.DELETE,"/api/ucenici").hasRole("ADMIN")
				.antMatchers("/api/ucenici/uploadDokumenti").hasRole("ADMIN")
				.anyRequest().authenticated();
	
		http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
		http.cors(); //za Access-Control-Allow-Origin error
	}
	
	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception{
		return super.authenticationManagerBean();
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	

}
