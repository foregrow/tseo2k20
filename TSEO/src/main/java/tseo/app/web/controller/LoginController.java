package tseo.app.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tseo.app.authmodel.AuthenticationRequest;
import tseo.app.authmodel.AuthenticationResponse;
import tseo.app.entity.Korisnik;
import tseo.app.enums.UlogaKorisnika;
import tseo.app.enums.UlogaNastavnika;
import tseo.app.service.KorisnikService;
import tseo.app.util.JwtUtil;


@CrossOrigin(origins ="*",allowedHeaders = "*")
@RestController
public class LoginController {

	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private JwtUtil jwtTokenUtil;
	
	@Autowired
	private KorisnikService userDetailsService;
	
	
	@RequestMapping(value ="/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception{
		
		try {
			
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(authenticationRequest.getKorisnickoIme(), authenticationRequest.getLozinka())
			);
		}catch (BadCredentialsException e) {
			throw new Exception ("netacni podaci username/password", e);
		}
		final UserDetails userDetails = userDetailsService
				.loadUserByUsername(authenticationRequest.getKorisnickoIme());
		final String jwt = jwtTokenUtil.generateToken(userDetails);
		Korisnik kor = userDetailsService.findByKorisnickoIme(authenticationRequest.getKorisnickoIme());
		String uloga = "GRESKA";
		if(kor.getUloga() == UlogaKorisnika.ROLE_ADMIN)
			uloga = "ROLE_ADMIN";
		else {
			if(kor.getUloga() == UlogaKorisnika.ROLE_KORISNIK) {
				if(kor.getUcenik() != null)
					uloga = "ROLE_UCENIK";
				else if(kor.getNastavnik() != null) {
					if(kor.getNastavnik().getUloga().equals(UlogaNastavnika.PROFESOR)) {
						uloga = "ROLE_NASTAVNIK";
					}else if(kor.getNastavnik().getUloga().equals(UlogaNastavnika.ASISTENT)) {
						uloga = "ROLE_ASISTENT";
					}else if(kor.getNastavnik().getUloga().equals(UlogaNastavnika.DEMONSTRATOR)) {
						uloga = "ROLE_DEMONSTRATOR";
					}
				}
					
				
			}
		}
		
		
		
		
		return ResponseEntity.ok(new AuthenticationResponse(jwt,uloga));
	}
	
}
