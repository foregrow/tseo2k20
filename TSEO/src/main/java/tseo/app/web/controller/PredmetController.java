package tseo.app.web.controller;

import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tseo.app.entity.Nastavnik;
import tseo.app.entity.Predmet;
import tseo.app.entity.Smer;
import tseo.app.entity.Ucenik;
import tseo.app.service.NastavnikService;
import tseo.app.service.PredmetService;
import tseo.app.service.SmerService;
import tseo.app.service.UcenikService;
import tseo.app.web.dto.NastavnikDTO;
import tseo.app.web.dto.PredmetDTO;

@CrossOrigin(origins ="*",allowedHeaders = "*")
@RestController
@RequestMapping(value="api/predmeti")
public class PredmetController {
	
	@Autowired
	PredmetService ps;
	
	@Autowired
	NastavnikService ns;
	
	@Autowired
	SmerService ss;
	
	@Autowired
	UcenikService us;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<PredmetDTO>> getPredmeti() {
		List<Predmet> predmeti = ps.findAll();
		//convert courses to DTOs
		List<PredmetDTO> dtos = new ArrayList<>();
		for (Predmet s : predmeti) {
			dtos.add(new PredmetDTO(s));
		}
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@RequestMapping(value="/predmetiUcenici/{korIme}",method = RequestMethod.GET)
	public ResponseEntity<?> getPredmetiUcenici(@PathVariable String korIme) {
		
		Nastavnik nas = ns.getByKorisnikKorisnickoIme(korIme);
		if(nas==null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		List<Predmet> predmeti = ps.getPredmetiNastavnikPredaje(nas.getId());
		List<PredmetDTO> dtos = new ArrayList<PredmetDTO>();
		for(Predmet p : predmeti) {
			PredmetDTO pdto = new PredmetDTO(p);
			pdto.setToList(p.getUcenici());
			pdto.setToListKlk(p.getKolokvijumi());
			dtos.add(pdto);
		}
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<PredmetDTO> getPredmet(@PathVariable long id){
		Predmet predmet = ps.findOne(id);
		if(predmet == null){
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(new PredmetDTO(predmet), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST, consumes="application/json")
	public ResponseEntity<?> savePredmet(@RequestBody PredmetDTO predmetDTO){
		Predmet predmet = new Predmet();
		Smer smer = ss.findByOznakaSmera(predmetDTO.getSmer().getOznakaSmera());
		if(smer != null) {
			for(Predmet p: smer.getPredmeti()) {
				if(p.getNaziv().toLowerCase().equals(predmetDTO.getNaziv().toLowerCase()))
					return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);	
			}
			predmet.setSmer(smer);
			for(Ucenik u : smer.getUcenici()) {
				predmet.getUcenici().add(u);
			}
				
			
		}else
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);	
			
		predmet.setNaziv(predmetDTO.getNaziv());
		predmet.setBrojECTSBodova(predmetDTO.getBrojECTSBodova());
		
		predmet = ps.save(predmet);
		return new ResponseEntity<>(new PredmetDTO(predmet), HttpStatus.OK);	
	}
	
	@RequestMapping(method=RequestMethod.PUT, consumes="application/json")
	public ResponseEntity<PredmetDTO> updatePredmet(@RequestBody PredmetDTO predmetDTO){
		//a course must exist
		Predmet predmet = ps.findOne(predmetDTO.getId());
		if (predmet == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		System.out.println(predmet.getNaziv());
		predmet.setBrojECTSBodova(predmetDTO.getBrojECTSBodova());
	
		ps.save(predmet);
		return new ResponseEntity<>(new PredmetDTO(predmet), HttpStatus.OK);	
	}
	
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<?> deletePredmet(@PathVariable Long id){
		Predmet predmet = ps.findOne(id);
		if (predmet != null){
			ps.remove(id);
			List<Predmet> predmeti = ps.findAll();
			List<PredmetDTO> dtos = new ArrayList<>();
			for(Predmet p : predmeti)
				dtos.add(new PredmetDTO(p));
			
			return new ResponseEntity<>(dtos,HttpStatus.OK);
		} else {		
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		}
		
	}
	
	@RequestMapping(value="/nastavnikNePredaje/{id}",method = RequestMethod.GET)
	public ResponseEntity<List<PredmetDTO>> getPredmetiNastavnikNePredaje(@PathVariable long id) {
		List<Predmet> predmetiNastavnikNePredaje = ps.getPredmetiNastavnikNePredaje(id);
		
		List<PredmetDTO> dtos = new ArrayList<>();
		for (Predmet s : predmetiNastavnikNePredaje) {
			dtos.add(new PredmetDTO(s));
		}
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@RequestMapping(value="/nastavniciPredaju",method = RequestMethod.GET)
	public ResponseEntity<List<PredmetDTO>> getPredmetiNastavniciPredaju() {
		List<Predmet> predmetiNastavniciPredaju = ps.getPredmetiNastavniciPredaju();
		
		List<PredmetDTO> dtos = new ArrayList<>();
		for (Predmet s : predmetiNastavniciPredaju) {
			dtos.add(new PredmetDTO(s));
		}
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@RequestMapping(value="/nastavnikPredaje/{nid}",method = RequestMethod.GET)
	public ResponseEntity<List<PredmetDTO>> getPredmetiNastavnikPredaje(@PathVariable long nid) {
		List<Predmet> predmeti = ps.getPredmetiNastavnikPredaje(nid);
		
		List<PredmetDTO> dtos = new ArrayList<>();
		for (Predmet s : predmeti) {
			dtos.add(new PredmetDTO(s));
		}
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@RequestMapping(value="/nepolozeniPredmeti/{smerId}/{ucenikId}", method=RequestMethod.GET)
	public ResponseEntity<?> getNepolozeniPredmeti(@PathVariable long smerId,@PathVariable long ucenikId){
		List<Predmet> predmeti = new ArrayList<Predmet>();
		List<PredmetDTO> dtos = new ArrayList<PredmetDTO>();
		predmeti = ps.getNepolozeniPredmeti(smerId, ucenikId);
		
		for(Predmet p : predmeti) {

			Nastavnik nastavnik = ns.getNastavnikByPredmet(p.getId());
			PredmetDTO dto = new PredmetDTO(p);
			if(nastavnik != null)
				dto.setProfesor(new NastavnikDTO(nastavnik));
			
			dtos.add(dto);
		}
		
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@RequestMapping(value="/predmetiZaPrijavu/{smerId}/{ucenikId}/{iRok}", method=RequestMethod.GET)
	public ResponseEntity<?> getPredmetiZaPrijavu(@PathVariable long smerId,@PathVariable long ucenikId,@PathVariable long iRok){
		List<Predmet> predmeti = new ArrayList<Predmet>();
		List<PredmetDTO> dtos = new ArrayList<PredmetDTO>();
		predmeti = ps.getPredmetiZaPrijavu(smerId, ucenikId, iRok);
		
		for(Predmet p : predmeti) {
			
			Nastavnik nastavnik = ns.getNastavnikByPredmet(p.getId());
			PredmetDTO dto = new PredmetDTO(p);
			if(nastavnik != null)
				dto.setProfesor(new NastavnikDTO(nastavnik));
			
			dtos.add(dto);
		}
		
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@RequestMapping(value="/prijavljeniPredmetiZaIspit/{ucenikId}/{iRok}", method=RequestMethod.GET)
	public ResponseEntity<?> getPrijavljeniPredmetiZaIspit(@PathVariable long ucenikId,@PathVariable long iRok){
		List<Predmet> predmeti = new ArrayList<Predmet>();
		List<PredmetDTO> dtos = new ArrayList<PredmetDTO>();
		predmeti = ps.getPrijavljeniPredmetiZaIspit(ucenikId,iRok);
		
		for(Predmet p : predmeti) {
			
			Nastavnik nastavnik = ns.getNastavnikByPredmet(p.getId());
			PredmetDTO dto = new PredmetDTO(p);
			if(nastavnik != null)
				dto.setProfesor(new NastavnikDTO(nastavnik));
			
			dtos.add(dto);
		}
		
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}

}
