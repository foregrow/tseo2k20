package tseo.app.web.controller;

import java.util.ArrayList;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tseo.app.entity.Korisnik;
import tseo.app.entity.Nastavnik;
import tseo.app.entity.Ucenik;
import tseo.app.enums.UlogaKorisnika;
import tseo.app.service.KorisnikService;
import tseo.app.service.NastavnikService;
import tseo.app.service.UcenikService;
import tseo.app.util.PasswordBCrypt;
import tseo.app.web.dto.KorisnikDTO;

@CrossOrigin(origins ="*",allowedHeaders = "*")
@RestController
@RequestMapping(value="api/korisnici")
public class KorisnikController {
	
	@Autowired
	KorisnikService ks;
	
	@Autowired
	UcenikService us;
	
	@Autowired
	NastavnikService ns;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<KorisnikDTO>> getKorisnici() {
		List<Korisnik> korisnici = ks.findAll();
		
		List<KorisnikDTO> dtos = new ArrayList<>();
		for (Korisnik s : korisnici) {
			
			dtos.add(new KorisnikDTO(s));
		}
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<KorisnikDTO> getById(@PathVariable long id){
		
		Korisnik kor = ks.findOne(id);
		if(kor == null){
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(new KorisnikDTO(kor), HttpStatus.OK);
	}
	
	@RequestMapping(value="/korIme/{korisnickoIme}", method=RequestMethod.GET)
	public ResponseEntity<KorisnikDTO> getByKorisnickoIme(@PathVariable String korisnickoIme){
		
		Korisnik kor = ks.findByKorisnickoIme(korisnickoIme);
		if(kor == null){
			
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(new KorisnikDTO(kor), HttpStatus.OK);
	}
	
	@RequestMapping(value="/proveraPristupaNastavnika/{idTrazenog}/{korImeUlogovanog}", method=RequestMethod.GET)
	public ResponseEntity<?> proveraPristupaNastavnika(@PathVariable long idTrazenog,@PathVariable String korImeUlogovanog){
		
		Korisnik kor = ks.findByNastavnikIdAndKorisnickoIme(idTrazenog, korImeUlogovanog);
		if(kor == null){
			//ne sme da pristupi tudjim podacima
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(new KorisnikDTO(kor), HttpStatus.OK);
	}
	@RequestMapping(value="/proveraPristupaUcenika/{idTrazenog}/{korImeUlogovanog}", method=RequestMethod.GET)
	public ResponseEntity<?> proveraPristupaUcenika(@PathVariable long idTrazenog,@PathVariable String korImeUlogovanog){
		
		Korisnik kor = ks.findByUcenikIdAndKorisnickoIme(idTrazenog, korImeUlogovanog);
		if(kor == null){
			//ne sme da pristupi tudjim podacima
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(new KorisnikDTO(kor), HttpStatus.OK);
	}
	
	@RequestMapping(value="/{uceIndex}/{nasEmail}",method=RequestMethod.POST, consumes="application/json")
	public ResponseEntity<KorisnikDTO> save(@RequestBody KorisnikDTO korDTO,
			@PathVariable String uceIndex, @PathVariable String nasEmail){
		if(korDTO == null || korDTO.getKorisnickoIme().equals("") || korDTO.getKorisnickoIme() == null) {
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		}
		
		Korisnik kor = new Korisnik();
		Ucenik uce;
		Nastavnik nas;
		
		kor.setKorisnickoIme(korDTO.getKorisnickoIme());
		kor.setLozinka(PasswordBCrypt.hashPassword(korDTO.getLozinka()));
		kor.setUloga(korDTO.getUloga());
		if(korDTO.getUloga().equals(UlogaKorisnika.ROLE_KORISNIK)) {
			if(!uceIndex.equals("null")) {
				uce = us.getByIndex(uceIndex);
				if(uce != null) {
					kor.setUcenik(uce);	
				}
			}else if(!nasEmail.equals("null")) {
				nas = ns.getByEmail(nasEmail);
				if(nas != null) {
					kor.setNastavnik(nas);
				}
			}
		}
		ks.save(kor);
		return new ResponseEntity<>(new KorisnikDTO(kor), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<KorisnikDTO> update(@RequestBody KorisnikDTO korDTO){
        Korisnik kor = ks.findOne(korDTO.getId());
 
        if(kor == null) {
            return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
        }
        kor.setLozinka(PasswordBCrypt.hashPassword(korDTO.getLozinka()));
        ks.save(kor);
       
        return new ResponseEntity<>(new KorisnikDTO(kor), HttpStatus.OK);
    }
	
	@RequestMapping(value="/{idNastavnika}", method=RequestMethod.PUT)
	public ResponseEntity<KorisnikDTO> updateNastavnikPassword(@RequestBody KorisnikDTO korDTO, @PathVariable long idNastavnika){
		Korisnik kor = ks.findByNastavnikId(idNastavnika);
		if(kor == null) {
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		}
		
		kor.setLozinka(PasswordBCrypt.hashPassword(korDTO.getLozinka()));
		ks.save(kor);
		
		return new ResponseEntity<>(new KorisnikDTO(kor), HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<?> delete(@PathVariable Long id){
		//admin brise korisnika
		Korisnik kor = ks.findOne(id);
		if (kor != null){
			ks.remove(id);
			
			List<Korisnik> korisnici = ks.findAll();
			
			List<KorisnikDTO> dtos = new ArrayList<>();
			for (Korisnik s : korisnici) {	
				dtos.add(new KorisnikDTO(s));
			}
			return new ResponseEntity<>(dtos, HttpStatus.OK);
		} else {		
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		}
	}
	
	

}
