package tseo.app.web.controller;

import java.util.ArrayList;


import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tseo.app.entity.Ispit;
import tseo.app.entity.IspitniRok;
import tseo.app.service.IspitniRokService;
import tseo.app.web.dto.IspitDTO;
import tseo.app.web.dto.IspitniRokDTO;


@CrossOrigin(origins ="*",allowedHeaders = "*")
@RestController
@RequestMapping(value="api/ispitniRok")
public class IspitniRokController {

	
	@Autowired
	IspitniRokService irs;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> getAll() {
		List<IspitniRok> rokovi = irs.getAll();
		
		List<IspitniRokDTO> dtos = new ArrayList<>();
		for (IspitniRok ir : rokovi) {
			dtos.add(new IspitniRokDTO(ir));
		}
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> getById(@PathVariable long id){

		IspitniRok ir = irs.getIspitniRokById(id);
		if(ir == null)
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		IspitniRokDTO dto = new IspitniRokDTO(ir);
		for(Ispit ii : ir.getIspiti())
			dto.getIspiti().add(new IspitDTO(ii));
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	@RequestMapping(value="/trenutniRok",method = RequestMethod.GET)
	public ResponseEntity<?> getTrenutniRok() {
		
		Date currentDate = Calendar.getInstance().getTime();
		IspitniRok is = irs.getTrenutniRok(currentDate);
		if(is == null) {
			System.out.println("is je null");
			return new ResponseEntity<>(null,HttpStatus.OK);

		}
		System.out.println(is.getNazivRoka());
		return new ResponseEntity<>(new IspitniRokDTO(is), HttpStatus.OK);
	}
	@RequestMapping(method=RequestMethod.PUT, consumes="application/json")
	public ResponseEntity<?> update(@RequestBody IspitniRokDTO dto){

		IspitniRok irok = irs.getIspitniRokById(dto.getId());
		if(irok == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		irok.setPocetakRoka(dto.getPocetakRoka());
		irok.setKrajRoka(dto.getKrajRoka());
		irs.save(irok);
		IspitniRokDTO zaSlanje = new IspitniRokDTO(irok);
		return new ResponseEntity<>(zaSlanje,HttpStatus.OK);	
	}
	
	
}
