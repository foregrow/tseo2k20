package tseo.app.web.controller;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import tseo.app.entity.Dokument;
import tseo.app.entity.FinansijskaKartica;
import tseo.app.entity.Predmet;
import tseo.app.entity.Smer;
import tseo.app.entity.SpisakUplata;
import tseo.app.entity.Ucenik;
import tseo.app.service.DokumentService;
import tseo.app.service.FinansijskaKarticaService;
import tseo.app.service.PredmetService;
import tseo.app.service.SmerService;
import tseo.app.service.SpisakUplataService;
import tseo.app.service.UcenikService;
import tseo.app.web.dto.DokumentDTO;
import tseo.app.web.dto.FinansijskaKarticaDTO;
import tseo.app.web.dto.SpisakUplataDTO;
import tseo.app.web.dto.UcenikDTO;

@PreAuthorize ("hasRole('ROLE_ADMIN')")
@CrossOrigin(origins ="*",allowedHeaders = "*")
@RestController
@RequestMapping(value="api/ucenici")
public class UcenikController {

	@Autowired
	UcenikService us;
	
	@Autowired
	SmerService ss;
	
	@Autowired
	FinansijskaKarticaService fks;
	
	@Autowired
	PredmetService ps;
	
	@Autowired
	SpisakUplataService sus;
	
	@Autowired
	DokumentService ds;
	
	public static String uploadDirectory = System.getProperty("user.dir")+"\\data\\";
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<UcenikDTO>> getAll() {
		List<Ucenik> ucenici = us.findAll();
		
		List<UcenikDTO> dtos = new ArrayList<>();
		for (Ucenik uc : ucenici) {
			dtos.add(new UcenikDTO(uc));
		}
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<UcenikDTO> getById(@PathVariable long id){
		Ucenik uce = us.findOne(id);
		if(uce == null){
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(new UcenikDTO(uce), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST, consumes="application/json")
	public ResponseEntity<?> save(
			@RequestBody FinansijskaKarticaDTO fkdto){
	
		String indexUcenika = us.createIndex(fkdto.getUcenik().getSmer().getOznakaSmera(), fkdto.getUcenik().getGodinaUpisa());
		Ucenik uc = new Ucenik();
		uc.setIme(fkdto.getUcenik().getIme());
		uc.setPrezime(fkdto.getUcenik().getPrezime());
		uc.setGodinaUpisa(fkdto.getUcenik().getGodinaUpisa());
		uc.setGodinaStudija(fkdto.getUcenik().getGodinaStudija());
		uc.setDrzavaRodjenja(fkdto.getUcenik().getDrzavaRodjenja());
		uc.setMestoRodjenja(fkdto.getUcenik().getMestoRodjenja());
		uc.setDatumRodjenja(fkdto.getUcenik().getDatumRodjenja());
		uc.setPol(fkdto.getUcenik().getPol());
		uc.setNacinFinansiranja(fkdto.getUcenik().getNacinFinansiranja());
		uc.setEmail(fkdto.getUcenik().getEmail());
		uc.setAdresa(fkdto.getUcenik().getAdresa());
		uc.setUkupnoECTSBodova(0);
		uc.setProsecnaOcena(0);
		if(indexUcenika.equals("")) {
			System.out.println("indexUcenika prazan string ");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		uc.setIndex(indexUcenika);
		Smer smer = ss.findByOznakaSmera(fkdto.getUcenik().getSmer().getOznakaSmera());
		if(smer != null) {
			uc.setSmer(smer);
		}
		us.save(uc);
		for(Predmet p: smer.getPredmeti()) {
			p.getUcenici().add(uc);
			ps.save(p);
		}

		String newBrojKartice = fks.createBrojKartice();
		
		if(newBrojKartice.equals("")) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		
		FinansijskaKartica fk = new FinansijskaKartica();
		fk.setBrojKartice(newBrojKartice);
		fk.setSuma(0);
		fk.setZiroRacun(fkdto.getZiroRacun());
		fk.setBrojModela(fkdto.getBrojModela());
		fk.setPozivNaBroj(fkdto.getPozivNaBroj());
		fk.setUcenik(uc);
		
		fks.save(fk);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.PUT, consumes="application/json")
	public ResponseEntity<?> update(@RequestBody FinansijskaKarticaDTO fdto){
		FinansijskaKartica fk = fks.findByUcenikId(fdto.getUcenik().getId());
		Ucenik uc = us.findOne(fdto.getUcenik().getId());
		System.out.println("backkkkkkkkkkk");
		if(uc == null) {
			System.out.println("uc==null");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		uc.setIme(fdto.getUcenik().getIme());
		uc.setPrezime(fdto.getUcenik().getPrezime());
		uc.setGodinaUpisa(fdto.getUcenik().getGodinaUpisa());
		uc.setGodinaStudija(fdto.getUcenik().getGodinaStudija());
		uc.setDrzavaRodjenja(fdto.getUcenik().getDrzavaRodjenja());
		uc.setMestoRodjenja(fdto.getUcenik().getMestoRodjenja());
		uc.setAdresa(fdto.getUcenik().getAdresa());
		uc.setDatumRodjenja(fdto.getUcenik().getDatumRodjenja());
		uc.setEmail(fdto.getUcenik().getEmail());
		uc.setPol(fdto.getUcenik().getPol());
		uc.setNacinFinansiranja(fdto.getUcenik().getNacinFinansiranja());
		
		Smer smer = ss.findByOznakaSmera(fdto.getUcenik().getSmer().getOznakaSmera());
		if(smer != null) {

			for(Predmet p: uc.getPredmeti())
				p.getUcenici().remove(uc);
	
			uc.setSmer(smer);
			
			for(Predmet p: smer.getPredmeti()) {
				p.getUcenici().add(uc);
				ps.save(p);
			}
			String indexUcenika = us.createIndex(smer.getOznakaSmera(), uc.getGodinaUpisa());
				uc.setIndex(indexUcenika);
			
			us.save(uc);
		}
		fk.setBrojModela(fdto.getBrojModela());
		fk.setPozivNaBroj(fdto.getPozivNaBroj());
		fk.setZiroRacun(fdto.getZiroRacun());
		fk.setSuma(fdto.getSuma());
		fks.save(fk);
		
		return new ResponseEntity<>(new FinansijskaKarticaDTO(fk), HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<?> delete(@PathVariable Long id){
		//admin brise
		Ucenik ucenik = us.findOne(id);
		if (ucenik != null){
			us.remove(id);
			List<Ucenik> ucenici = us.findAll();
			List<UcenikDTO> dtos = new ArrayList<>();
			for(Ucenik n : ucenici) {
				dtos.add(new UcenikDTO(n));
			}
			return new ResponseEntity<>(dtos, HttpStatus.OK);
		} else {		
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/dokument/{did}", method=RequestMethod.DELETE)
	public ResponseEntity<?> deleteDokument(@PathVariable Long did){

		Dokument dok = ds.getById(did).get();
		//Ucenik uc = us.findOne(dok.getUcenik().getId());
		
		if(dok != null)
			ds.remove(dok.getId());
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	

	@RequestMapping(value="/spisakUplata/{uid}",method = RequestMethod.GET)
	public ResponseEntity<?> spisakUplataUcenika(@PathVariable long uid) {
		List<SpisakUplata> uplate = sus.findByUcenikId(uid);
		
		List<SpisakUplataDTO> dtos = new ArrayList<>();
		for (SpisakUplata su : uplate)
			dtos.add(new SpisakUplataDTO(su));
		
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@RequestMapping(value="/notInKorisnik",method = RequestMethod.GET)
	public ResponseEntity<List<UcenikDTO>> getAllFromUcenik() {
		List<Ucenik> ucenici = us.getAllFromUcenik();
		
		List<UcenikDTO> dtos = new ArrayList<>();
		for (Ucenik uc : ucenici) {
			dtos.add(new UcenikDTO(uc));
		}
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}

	
	@RequestMapping(value="/downloadFile/{id}",method = RequestMethod.GET)
	public ResponseEntity<ByteArrayResource> downloadFile(@PathVariable long id){
		Dokument dok = ds.getById(id).get();
		DokumentDTO dto = new DokumentDTO(dok);
		//Access-Control-Expose-Headers: *
		return ResponseEntity.ok()
				.contentType(MediaType.parseMediaType(dto.getTip()))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment:filename=\""+dto.getNaziv()+"\"")
				.header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, "*")
				.body(new ByteArrayResource(dto.getDokument()));
		
	}
	@PostMapping("/uploadDokumenti")
	public ResponseEntity<?> upload(@RequestParam("uid") long uid,@RequestParam("files") MultipartFile[] files) throws IOException {
		
		Ucenik u = us.findOne(uid);
		if(u == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		System.out.println(files.length);
		for(MultipartFile file : files) {
			System.out.println(file.getOriginalFilename());
			ds.saveFile(file,u);
		}
		List<Dokument> dokumenti = ds.getByUcenikId(u.getId());
		UcenikDTO dto = new UcenikDTO();
		for(Dokument dok : dokumenti) {
			dto.getDokumenti().add(new DokumentDTO(dok));
		}
		return new ResponseEntity<>(dto,HttpStatus.OK); 
	}
	
	
	
}
