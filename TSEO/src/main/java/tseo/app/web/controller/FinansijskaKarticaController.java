package tseo.app.web.controller;

import java.util.ArrayList;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tseo.app.entity.Dokument;
import tseo.app.entity.FinansijskaKartica;
import tseo.app.entity.Predmet;
import tseo.app.service.DokumentService;
import tseo.app.service.FinansijskaKarticaService;
import tseo.app.service.PredmetService;
import tseo.app.service.UcenikService;
import tseo.app.web.dto.DokumentDTO;
import tseo.app.web.dto.FinansijskaKarticaDTO;
import tseo.app.web.dto.PredmetDTO;

@CrossOrigin(origins ="*",allowedHeaders = "*")
@RestController
@RequestMapping(value="api/finansijskaKartica")
public class FinansijskaKarticaController {

	@Autowired
	FinansijskaKarticaService fks;
	
	@Autowired
	PredmetService ps;
	
	@Autowired
	UcenikService us;
	
	@Autowired
	DokumentService ds;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<FinansijskaKarticaDTO>> getAll() {
		List<FinansijskaKartica> listaFk = fks.findAll();
		
		List<FinansijskaKarticaDTO> dtos = new ArrayList<>();
		for (FinansijskaKartica f : listaFk) {
			dtos.add(new FinansijskaKarticaDTO(f));
		}
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@RequestMapping(value="/ucenik/{ucenikId}", method=RequestMethod.GET)
	public ResponseEntity<?> getByUcenikId(@PathVariable long ucenikId){
		FinansijskaKartica fkfk = fks.findByUcenikId(ucenikId);
		if(fkfk == null){
			
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		}
		FinansijskaKarticaDTO dto = new FinansijskaKarticaDTO(fkfk);
		List<Dokument> dokumenti = ds.getByUcenikId(ucenikId);
		List<Predmet> pohadja = ps.getPredmetiUcenikPohadja(ucenikId);
		for(Predmet p : pohadja)
			dto.getUcenik().getPohadja().add(new PredmetDTO(p));
		for(Dokument d : dokumenti)
			dto.getUcenik().getDokumenti().add(new DokumentDTO(d));
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
}