package tseo.app.web.controller;

import java.util.ArrayList;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tseo.app.entity.Korisnik;
import tseo.app.entity.Nastavnik;
import tseo.app.entity.Predmet;
import tseo.app.entity.Smer;
import tseo.app.service.KorisnikService;
import tseo.app.service.NastavnikService;
import tseo.app.service.PredmetService;
import tseo.app.service.SmerService;
import tseo.app.web.dto.KorisnikDTO;
import tseo.app.web.dto.NastavnikDTO;
import tseo.app.web.dto.PredmetDTO;
import tseo.app.web.dto.SmerDTO;

@CrossOrigin(origins ="*",allowedHeaders = "*")
@RestController
@RequestMapping(value="api/nastavnici")
public class NastavnikController {

	@Autowired
	NastavnikService ns;
	
	@Autowired
	KorisnikService ks;
	
	@Autowired
	PredmetService ps;
	
	@Autowired  
	SmerService ss;
	
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<NastavnikDTO>> getAll() {
		List<Nastavnik> nastavnici = ns.findAll();
		
		List<NastavnikDTO> dtos = new ArrayList<>();
		for (Nastavnik n : nastavnici) {
			dtos.add(new NastavnikDTO(n));
		}
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<NastavnikDTO> getById(@PathVariable long id){

		Nastavnik nas = ns.findOne(id);
		if(nas == null){
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		}
		Smer s = ss.findByNastavnikId(nas.getId());
		SmerDTO smerdto = null;
		
		if(s != null) {
			smerdto = new SmerDTO(s);
		}
		Korisnik k = ks.findByNastavnikId(nas.getId());
		KorisnikDTO korisnikdto = null;
		if(k != null) {
			korisnikdto = new KorisnikDTO(k);
		}
		List<Predmet> predaje = ps.getPredmetiNastavnikPredaje(id);
		List<PredmetDTO> predajedto = new ArrayList<PredmetDTO>();
		
		if(predaje != null) {
			for(Predmet p : predaje) {
				PredmetDTO pdto = new PredmetDTO(p);
				//pdto.setToList(p.getUcenici());
				predajedto.add(pdto);
			}
		}
		NastavnikDTO dto = new NastavnikDTO(nas);
		dto.setKorisnik(korisnikdto);
		dto.setSmer(smerdto);
		dto.setPredaje(predajedto);
		
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST, consumes="application/json")
	public ResponseEntity<NastavnikDTO> save(@RequestBody NastavnikDTO dto){
		Nastavnik nas = new Nastavnik();
		nas.setIme(dto.getIme());
		nas.setPrezime(dto.getPrezime());
		nas.setEmail(dto.getEmail());
		nas.setUloga(dto.getUloga());
		
		ns.save(nas);
		
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.PUT, consumes="application/json")
	public ResponseEntity<NastavnikDTO> update(@RequestBody NastavnikDTO nasDTO){
		Nastavnik nas = ns.findOne(nasDTO.getId());
		if(nas == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		nas.setIme(nasDTO.getIme());
		nas.setPrezime(nasDTO.getPrezime());
		nas.setEmail(nasDTO.getEmail());
		nas.setUloga(nasDTO.getUloga());
		
		nas = ns.save(nas);
		NastavnikDTO dto = new NastavnikDTO(nas);
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<?> delete(@PathVariable Long id){
		//admin brise
		Nastavnik nas = ns.findOne(id);
		if (nas != null){
			ns.remove(id);
			List<Nastavnik> nastavnici = ns.findAll();
			List<NastavnikDTO> dtos = new ArrayList<>();
			for(Nastavnik n : nastavnici) {
				dtos.add(new NastavnikDTO(n));
			}
			return new ResponseEntity<>(dtos, HttpStatus.OK);
		} else {		
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/notInKorisnik",method = RequestMethod.GET)
	public ResponseEntity<List<NastavnikDTO>> getAllFromNastavnik() {
		List<Nastavnik> nastavnici = ns.getAllFromNastavnik();
		
		List<NastavnikDTO> dtos = new ArrayList<>();
		for (Nastavnik nas : nastavnici) {
			dtos.add(new NastavnikDTO(nas));
		}
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	
	@RequestMapping(value="/sefKatedreNull",method = RequestMethod.GET)
	public ResponseEntity<List<NastavnikDTO>> getAllWhereSefKatedreNull() {
		List<Nastavnik> nastavnici = ns.getAllWhereSefKatedreNull();
		
		List<NastavnikDTO> dtos = new ArrayList<>();
		for (Nastavnik nas : nastavnici) {
			dtos.add(new NastavnikDTO(nas));
		}
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@RequestMapping(value="/addPredmeteNastavniku",method=RequestMethod.PUT, consumes="application/json")
	public ResponseEntity<?> updatePredmeta(@RequestBody NastavnikDTO ndto){

		Nastavnik nas = ns.findOne(ndto.getId());
		if(nas == null)
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		
		for(PredmetDTO dto : ndto.getPredmeti()) {
			Predmet p = ps.findOne(dto.getId());
			
			if(p == null)
				return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
			p.getNastavnici().add(nas);
			ps.save(p);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	
}
