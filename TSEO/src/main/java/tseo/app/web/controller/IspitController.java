package tseo.app.web.controller;

import java.text.DateFormat;




import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tseo.app.entity.FinansijskaKartica;
import tseo.app.entity.Ispit;
import tseo.app.entity.IspitniRok;
import tseo.app.entity.Kolokvijum;
import tseo.app.entity.Nastavnik;
import tseo.app.entity.Predmet;
import tseo.app.entity.SpisakUplata;
import tseo.app.entity.Ucenik;
import tseo.app.enums.StatusIspita;
import tseo.app.service.FinansijskaKarticaService;
import tseo.app.service.IspitService;
import tseo.app.service.IspitniRokService;
import tseo.app.service.KolokvijumService;
import tseo.app.service.NastavnikService;
import tseo.app.service.PredmetService;
import tseo.app.service.SpisakUplataService;
import tseo.app.service.UcenikService;
import tseo.app.web.dto.IspitDTO;
import tseo.app.web.dto.NastavnikDTO;
import tseo.app.web.dto.PredmetDTO;

@CrossOrigin(origins ="*",allowedHeaders = "*")
@RestController
@RequestMapping(value="api/ispiti")
public class IspitController {

	@Autowired
	IspitService is;
	@Autowired
	IspitniRokService isr;
	@Autowired
	PredmetService psr;
	@Autowired
	UcenikService urs;
	@Autowired
	FinansijskaKarticaService fks;
	@Autowired
	NastavnikService ns;
	@Autowired
	SpisakUplataService sus;
	@Autowired
	KolokvijumService ks;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<IspitDTO>> getAll() {
		List<Ispit> ispiti = is.findAll();
		
		List<IspitDTO> dtos = new ArrayList<>();
		for (Ispit i : ispiti) {
			IspitDTO is = new IspitDTO(i);
			Nastavnik nastavnik = ns.getNastavnikByPredmet(i.getPredmet().getId());
			if(nastavnik != null)
				is.getPredmet().setProfesor(new NastavnikDTO(nastavnik));
			dtos.add(is);
		}
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<IspitDTO> getById(@PathVariable long id){
		//System.out.println("uslo u /{id} ispit");
		Ispit ispit = is.findOne(id);
		if(ispit == null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(new IspitDTO(ispit), HttpStatus.OK);
	}
	
	
	@RequestMapping(value="/uIspitnomRoku/{irid}", method=RequestMethod.GET)
	public ResponseEntity<?> findByIspitniRokId(@PathVariable long irid){
		//System.out.println("uslo u /{id} ispit");
		List<Ispit> ispiti = is.findByIspitniRokId(irid);
		
		List<IspitDTO> dtos = new ArrayList<IspitDTO>();
		for(Ispit i : ispiti) {
			dtos.add(new IspitDTO(i));
		}
		
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@RequestMapping(value="/istorijaPolaganja/{uid}", method=RequestMethod.GET)
	public ResponseEntity<?> istorijaPolaganjaUcenika(@PathVariable long uid){
		
		List<Ispit> ispiti = is.findByUcenikIdAndStatus(uid, StatusIspita.ZAVRSEN);
		
		List<IspitDTO> dtos = new ArrayList<IspitDTO>();
		for(Ispit i : ispiti) {
			IspitDTO is = new IspitDTO(i);
			Nastavnik nastavnik = ns.getNastavnikByPredmet(i.getPredmet().getId());
			if(nastavnik != null)
				is.getPredmet().setProfesor(new NastavnikDTO(nastavnik));
			dtos.add(is);
		}
		
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{ispitniRokId}/{ucenikId}/{suma}",method=RequestMethod.POST, consumes="application/json")
	public ResponseEntity<?> save(@RequestBody IspitDTO dto, @PathVariable long ispitniRokId, @PathVariable long ucenikId,@PathVariable double suma){
		
		if(dto.getPrijavljeniPredmeti().isEmpty()) 
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		FinansijskaKartica fk = fks.findByUcenikId(ucenikId);
		if(fk == null)
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		double fsuma = fk.getSuma() - suma;
		if(fsuma < 0)
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		fk.setSuma(fsuma);
		fks.save(fk);
		
		for(Long id : dto.getPrijavljeniPredmeti()) {
			Predmet p = psr.findOne(id);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			//java.sql.Date sqlDate = new java.sql.Date(date.getTime());
			//System.out.println(sqlDate);
			Ispit ispit = new Ispit();
			IspitniRok irok;
			irok = isr.getIspitniRokById(ispitniRokId);
			if(irok != null) 
				ispit.setIspitniRok(irok);
			Ucenik u;
			u = urs.findOne(ucenikId);
			if(u != null)
				ispit.setUcenik(u);
			
			ispit.setBodoviTeorija(0);
			ispit.setBodoviVezbe(0);
			ispit.setCena(200);
			ispit.setDatumPrijave(dateFormat.format(date));
			ispit.setOcena(0);
			ispit.setPolozen(false);
			ispit.setDatumPolaganja(p.getDatumPolaganja());
			ispit.setPredmet(p);
			ispit.setStatus(StatusIspita.PRIJAVLJEN);
			is.save(ispit);
			
			
			SpisakUplata su = new SpisakUplata();
			su.setDodato(false);
			su.setSuma(200);
			String opis = "Prijava ispita (" + p.getNaziv() + ")";
			su.setSvrha(opis);
			su.setUcenik(u);
			sus.save(su);
		}
		
		return new ResponseEntity<>(HttpStatus.CREATED);	
	}
	
	
	@RequestMapping(value="/posledjivanjeOcene",method=RequestMethod.PUT, consumes="application/json")
	public ResponseEntity<?> posledjivanjeOcene(@RequestBody IspitDTO dto){

		Ucenik uc = urs.findOne(dto.getUcenik().getId());
		if(uc == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		Ispit ispit = is.findByIspitniRokIdAndUcenikIdAndPredmetIdAndStatusAndPolozen(dto.getIspitniRok().getId(), dto.getUcenik().getId(), dto.getPredmet().getId(), StatusIspita.PRIJAVLJEN, false);
		if(ispit == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		for(Kolokvijum klk : uc.getKolokvijumi()) {
			if(klk.getPredmet().getId() == ispit.getPredmet().getId()) {
				ispit.getKolokvijumi().add(klk);
				int bodoviVezbe = ispit.getBodoviVezbe() + (int) klk.getBodovi();
				ispit.setBodoviVezbe(bodoviVezbe);
			}
		}
		ispit.setBodoviTeorija(dto.getBodoviTeorija());
		int ocena = is.createOcena(ispit.getBodoviTeorija(),ispit.getBodoviVezbe());
		ispit.setOcena(ocena);
		if(ocena <= 5)
			ispit.setPolozen(false);
		else {
			ispit.setPolozen(true);
			int bodovi = uc.getUkupnoECTSBodova() + ispit.getPredmet().getBrojECTSBodova();
			double prosecna = urs.prosecnaOcena(ocena, uc);
			uc.setProsecnaOcena(prosecna);
			uc.setUkupnoECTSBodova(bodovi);
			urs.save(uc);
		}
		ispit.setStatus(StatusIspita.ZAVRSEN);
		is.save(ispit);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(value="/addDatumPolaganja",method=RequestMethod.PUT, consumes="application/json")
	public ResponseEntity<?> update(@RequestBody IspitDTO ispitDTO){

		for(PredmetDTO dto : ispitDTO.getPredmetiDatumPromena()) {
			Predmet p = psr.findOne(dto.getId());
			if(p == null)
				return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
			p.setDatumPolaganja(dto.getDatumPolaganja());
			psr.save(p);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Long id){
		//admin brise
		Ispit ispit = is.findOne(id);
		if (ispit != null){
			is.remove(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {		
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/odjavaIspita/{uid}/{irid}", method=RequestMethod.PUT)
	public ResponseEntity<?> ispitiZaOdjavu(@RequestBody IspitDTO dto,@PathVariable long uid,@PathVariable long irid){
		
		if(dto.getPrijavljeniPredmeti().isEmpty()) 
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		int counter = 0;
		for(Long pid : dto.getPrijavljeniPredmeti()) {
			Ispit ispit = is.ispitZaOdjavu(uid, pid, irid);
			if(ispit != null) {
				String opis = "Odjava ispita (" + ispit.getPredmet().getNaziv() + ")";
				is.remove(ispit.getId());
				counter++;
				SpisakUplata su = new SpisakUplata();
				su.setDodato(true);
				su.setSuma(200);
				su.setSvrha(opis);
				su.setUcenik(ispit.getUcenik());
				sus.save(su);
			}	
		}
		FinansijskaKartica fk = fks.findByUcenikId(uid);
		double sumaZaDodavanje = counter*200;
		fk.setSuma(fk.getSuma() + sumaZaDodavanje);
		fks.save(fk);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(value="/uceniciPrijaviliIspit/{irid}/{nid}",method = RequestMethod.GET)
	public ResponseEntity<?> getUceniciPrijaviliIspit(@PathVariable long irid, @PathVariable long nid) {
		Nastavnik nastavnik = ns.findOne(nid);
		if(nastavnik == null)
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		
		List<PredmetDTO> dtos = new ArrayList<>();
		for(Predmet pred : nastavnik.getPredmeti()) {
			List<Ucenik> ucenici = is.getUceniciPrijaviliIspit(pred.getId(), irid);
			PredmetDTO pdto = new PredmetDTO(pred);
			pdto.ucenikToUcenikDTO(ucenici);
			dtos.add(pdto);
			
		}
		
		
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@RequestMapping(value="/nastavnikoviPredmeti/{irid}/{nid}",method = RequestMethod.GET)
	public ResponseEntity<?> getIspitiZaNastavnikovePredmete(@PathVariable long irid, @PathVariable long nid) {
		
		Nastavnik nastavnik = ns.findOne(nid);
		if(nastavnik == null)
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		//NastavnikDTO dto = new NastavnikDTO(nastavnik);
		List<Ispit> sviIspiti = new ArrayList<>();
		for(Predmet pred : nastavnik.getPredmeti()) {
			List<Ispit> ispiti = is.findByPredmetIdAndIspitniRokId(pred.getId(),irid);
			sviIspiti.addAll(ispiti);
		}
		List<IspitDTO> dtos = new ArrayList<IspitDTO>();
		
		for(Ispit is : sviIspiti)
			dtos.add(new IspitDTO(is));
		
		
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
}
