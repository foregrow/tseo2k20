package tseo.app.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tseo.app.entity.Kolokvijum;
import tseo.app.entity.Predmet;
import tseo.app.entity.Ucenik;
import tseo.app.service.KolokvijumService;
import tseo.app.service.PredmetService;
import tseo.app.service.UcenikService;
import tseo.app.web.dto.KolokvijumDTO;

@CrossOrigin(origins ="*",allowedHeaders = "*")
@RestController
@RequestMapping(value="api/kolokvijumi")
public class KolokvijumController {

	
	@Autowired
	KolokvijumService ks;
	
	@Autowired
	PredmetService ps;
	
	@Autowired
	UcenikService us;
	
	@RequestMapping(method=RequestMethod.POST, consumes="application/json")
	public ResponseEntity<?> save(@RequestBody KolokvijumDTO dto){
		Kolokvijum kol = new Kolokvijum();
		Ucenik uc = us.findOne(dto.getUcenik().getId());
		Predmet pred = ps.findOne(dto.getPredmet().getId());
		
		if(uc == null)
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		if(pred == null)
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);

		//int countKlks = ks.countBrojKlkova(uc.getId(), pred.getId());
		
			
		kol.setNaziv(dto.getNaziv());
		kol.setBodovi(dto.getBodovi());
		kol.setDatumPolaganja(dto.getDatumPolaganja());
		kol.setUcenik(uc);
		kol.setPredmet(pred);
		
		ks.save(kol);
		

		return new ResponseEntity<>(HttpStatus.OK);
	}
}
