package tseo.app.web.controller;

import java.util.ArrayList;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tseo.app.entity.Nastavnik;
import tseo.app.entity.Predmet;
import tseo.app.entity.Smer;
import tseo.app.service.NastavnikService;
import tseo.app.service.SmerService;
import tseo.app.web.dto.PredmetDTO;
import tseo.app.web.dto.SmerDTO;

@CrossOrigin(origins ="*",allowedHeaders = "*")
@RestController
@RequestMapping(value="api/smerovi")
public class SmerController {
	
	@Autowired
	SmerService ss;
	
	@Autowired
	NastavnikService ns;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<SmerDTO>> getAll() {
		List<Smer> smerovi = ss.findAll();
		
		List<SmerDTO> dtos = new ArrayList<>();
		for (Smer s : smerovi) {
			SmerDTO smerdto = new SmerDTO(s);
			smerdto.transformToPredmetiDTO(s.getPredmeti());
			dtos.add(smerdto);
		}
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<SmerDTO> getById(@PathVariable long id){
		Smer smer = ss.findOne(id);
		if(smer == null){
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		}
		SmerDTO dto = new SmerDTO(smer);
		for(Predmet p : smer.getPredmeti())
			dto.getPredmeti().add(new PredmetDTO(p));
		
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	@RequestMapping(value="/nazivPredmeta/{naziv}", method=RequestMethod.GET)
	public ResponseEntity<?> getByNazivPredmeta(@PathVariable String naziv){
		List<Smer> smerovi = ss.findSmeroviByPredmetNaziv(naziv);
		List<SmerDTO> dtos = new ArrayList<SmerDTO>();
		for(Smer s: smerovi) {
			dtos.add(new SmerDTO(s));
		}
		
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{emailNas}", method=RequestMethod.POST, consumes="application/json")
	public ResponseEntity<SmerDTO> save(@RequestBody SmerDTO smerDTO,@PathVariable String emailNas){
		Smer smer = new Smer();
		smer.setNaziv(smerDTO.getNaziv());
		smer.setBrojECTSBodova(smerDTO.getBrojECTSBodova());
		smer.setOznakaSmera(smerDTO.getOznakaSmera());
		Nastavnik nas;
		if(!emailNas.equals("null")) {
			nas = ns.getByEmail(emailNas);
			if(nas != null) 
				smer.setNastavnik(nas);
		}
		
		smer = ss.save(smer);
		return new ResponseEntity<>(new SmerDTO(smer), HttpStatus.CREATED);	
	}
	
	@RequestMapping(value="/{emailNas}", method=RequestMethod.PUT, consumes="application/json")
	public ResponseEntity<SmerDTO> update(@RequestBody SmerDTO smerDTO,@PathVariable String emailNas){
		
		Smer smer = ss.findOne(smerDTO.getId());
		if (smer == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		smer.setNaziv(smerDTO.getNaziv());
		smer.setBrojECTSBodova(smerDTO.getBrojECTSBodova());
		Nastavnik nas;
		if(!emailNas.equals("null")) {
			nas = ns.getByEmail(emailNas);
			if(nas != null) 
				smer.setNastavnik(nas);
		}
		smer = ss.save(smer);
		return new ResponseEntity<>(new SmerDTO(smer), HttpStatus.OK);	
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<?> delete(@PathVariable Long id){
		Smer smer = ss.findOne(id);
		if (smer != null){
			ss.remove(id);
			List<Smer> smerovi = ss.findAll();
			List<SmerDTO> dtos = new ArrayList<>();
			for(Smer s : smerovi) 
				dtos.add(new SmerDTO(s));
			
			return new ResponseEntity<>(dtos, HttpStatus.OK);
		}else	
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
	
		
	}

}
