package tseo.app.web.dto;

import java.util.ArrayList;

import java.util.List;

import tseo.app.entity.Nastavnik;
import tseo.app.enums.UlogaNastavnika;


public class NastavnikDTO {

	private long id;
	private String ime;
	private String prezime;
	private String email;
	private UlogaNastavnika uloga;
	
	private SmerDTO smer;
	
	private KorisnikDTO korisnik;
	
	private List<PredmetDTO> predaje = new ArrayList<PredmetDTO>();
	private List<PredmetDTO> predmeti = new ArrayList<PredmetDTO>();
	
	public NastavnikDTO() {
		
	}
	
	public NastavnikDTO(Nastavnik nas) {
			id = nas.getId();
			ime = nas.getIme();
			prezime = nas.getPrezime();
			email = nas.getEmail();
			uloga = nas.getUloga();
			/*if(nas.getSmer() != null)
				smer = new SmerDTO(nas.getSmer());
			else
				smer = new SmerDTO();*/
			/*if(nas.getKorisnik() != null)
				korisnik = new KorisnikDTO(nas.getKorisnik());	
			else
				korisnik = new KorisnikDTO();*/
			
	}
	
	

	public NastavnikDTO(long id, String ime, String prezime, String email, UlogaNastavnika uloga,SmerDTO smer,KorisnikDTO korisnik,List<PredmetDTO> predaje,List<PredmetDTO> predmeti) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.email = email;
		this.uloga = uloga;
		this.smer = smer;
		this.korisnik = korisnik;
		this.predaje = predaje;
		this.predmeti = predmeti;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public UlogaNastavnika getUloga() {
		return uloga;
	}

	public void setUloga(UlogaNastavnika uloga) {
		this.uloga = uloga;
	}

	public SmerDTO getSmer() {
		return smer;
	}

	public void setSmer(SmerDTO smer) {
		this.smer = smer;
	}

	public KorisnikDTO getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(KorisnikDTO korisnik) {
		this.korisnik = korisnik;
	}

	public List<PredmetDTO> getPredaje() {
		return predaje;
	}

	public void setPredaje(List<PredmetDTO> predaje) {
		this.predaje = predaje;
	}

	public List<PredmetDTO> getPredmeti() {
		return predmeti;
	}

	public void setPredmeti(List<PredmetDTO> predmeti) {
		this.predmeti = predmeti;
	}

}
