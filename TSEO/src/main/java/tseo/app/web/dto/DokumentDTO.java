package tseo.app.web.dto;

import java.io.File;
import java.nio.file.Files;

import org.springframework.web.multipart.MultipartFile;

import tseo.app.entity.Dokument;

public class DokumentDTO {

	private long id;
	private String naziv;
	private String tip;
	private String path;
	private byte[] dokument;
	private UcenikDTO ucenik;
	private MultipartFile[] fajlovi;
	
	public DokumentDTO() {
		
	}
	
	public DokumentDTO(Dokument dok) {
		id = dok.getId();
		naziv = dok.getNaziv();
		tip = dok.getTip();
		path = dok.getPath();
		dokument = dok.getDokument();
		if(dok.getUcenik()!= null)
			ucenik = new UcenikDTO(dok.getUcenik());
		else
			ucenik = new UcenikDTO();
	}


	public DokumentDTO(long id, String naziv, String tip, String path, UcenikDTO ucenik,byte[] dokument) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.tip = tip;
		this.path = path;
		this.ucenik = ucenik;
		this.dokument = dokument;
	}


	public byte[] getDokument() {
		return dokument;
	}

	public void setDokument(byte[] dokument) {
		this.dokument = dokument;
	}

	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getNaziv() {
		return naziv;
	}


	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}


	public String getTip() {
		return tip;
	}


	public void setTip(String tip) {
		this.tip = tip;
	}


	public String getPath() {
		return path;
	}


	public void setPath(String path) {
		this.path = path;
	}


	public UcenikDTO getUcenik() {
		return ucenik;
	}


	public void setUcenik(UcenikDTO ucenik) {
		this.ucenik = ucenik;
	}

	public MultipartFile[] getFajlovi() {
		return fajlovi;
	}

	public void setFajlovi(MultipartFile[] fajlovi) {
		this.fajlovi = fajlovi;
	}
	
	
}
