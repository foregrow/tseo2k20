package tseo.app.web.dto;





import tseo.app.entity.Kolokvijum;

public class KolokvijumDTO {

	
	private long id;
	private float bodovi;
	private String datumPolaganja;
	private String naziv; 
	
	private PredmetDTO predmet;
	private IspitDTO ispit;
	private UcenikDTO ucenik;
	
	public KolokvijumDTO() {
		
	}
	
	public KolokvijumDTO(Kolokvijum klk) {
		id = klk.getId();
		datumPolaganja = klk.getDatumPolaganja();
		bodovi = klk.getBodovi();
		naziv = klk.getNaziv();
		if(klk.getPredmet()!=null)
			predmet = new PredmetDTO(klk.getPredmet());
		else
			predmet = new PredmetDTO();
		if(klk.getIspit()!=null)
			ispit = new IspitDTO(klk.getIspit());
		else
			ispit = new IspitDTO();
		if(klk.getUcenik()!=null)
			ucenik = new UcenikDTO(klk.getUcenik());
		else
			ucenik = new UcenikDTO();
	}

	public KolokvijumDTO(long id, float bodovi, String datumPolaganja, PredmetDTO predmet, IspitDTO ispit,String naziv,
			UcenikDTO ucenik) {
		super();
		this.id = id;
		this.bodovi = bodovi;
		this.datumPolaganja = datumPolaganja;
		this.predmet = predmet;
		this.ispit = ispit;
		this.ucenik = ucenik;
		this.naziv = naziv;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public float getBodovi() {
		return bodovi;
	}

	public void setBodovi(float bodovi) {
		this.bodovi = bodovi;
	}

	public String getDatumPolaganja() {
		return datumPolaganja;
	}

	public void setDatumPolaganja(String datumPolaganja) {
		this.datumPolaganja = datumPolaganja;
	}

	public PredmetDTO getPredmet() {
		return predmet;
	}

	public void setPredmet(PredmetDTO predmet) {
		this.predmet = predmet;
	}

	public IspitDTO getIspit() {
		return ispit;
	}

	public void setIspit(IspitDTO ispit) {
		this.ispit = ispit;
	}

	public UcenikDTO getUcenik() {
		return ucenik;
	}

	public void setUcenik(UcenikDTO ucenik) {
		this.ucenik = ucenik;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	
	
}
