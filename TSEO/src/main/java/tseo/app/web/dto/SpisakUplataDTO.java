package tseo.app.web.dto;


import tseo.app.entity.SpisakUplata;

public class SpisakUplataDTO {
	
	private long id;
	private String svrha;
	private UcenikDTO ucenik;
	private double suma;
	private boolean dodato;
	public SpisakUplataDTO(SpisakUplata su) {
		this.id = su.getId();
		this.svrha = su.getSvrha();
		this.suma = su.getSuma();
		this.dodato = su.isDodato();
		if(su.getUcenik() != null)
			this.ucenik = new UcenikDTO(su.getUcenik());
		else
			this.ucenik = new UcenikDTO();
		
	}
	

	public SpisakUplataDTO(long id, String svrha, UcenikDTO ucenik, double suma, boolean dodato) {
		super();
		this.id = id;
		this.svrha = svrha;
		this.ucenik = ucenik;
		this.suma = suma;
		this.dodato = dodato;
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSvrha() {
		return svrha;
	}

	public void setSvrha(String svrha) {
		this.svrha = svrha;
	}

	public UcenikDTO getUcenik() {
		return ucenik;
	}

	public void setUcenik(UcenikDTO ucenik) {
		this.ucenik = ucenik;
	}

	public double getSuma() {
		return suma;
	}

	public void setSuma(double suma) {
		this.suma = suma;
	}


	public boolean isDodato() {
		return dodato;
	}


	public void setDodato(boolean dodato) {
		this.dodato = dodato;
	}
	
	
	
	
}
