package tseo.app.web.dto;


import tseo.app.entity.FinansijskaKartica;

public class FinansijskaKarticaDTO {

	private long id;
	private String brojKartice;
	private double suma;
	
	//
	private String ziroRacun;
	private String pozivNaBroj;
	private String brojModela;
	//
	
	private UcenikDTO ucenik;
	
	public FinansijskaKarticaDTO() {
		
	}
	
	public FinansijskaKarticaDTO(FinansijskaKartica fk) {
		id = fk.getId();
		brojKartice = fk.getBrojKartice();
		suma = fk.getSuma();
		ziroRacun = fk.getZiroRacun();
		pozivNaBroj = fk.getPozivNaBroj();
		brojModela = fk.getBrojModela();
		if(fk.getUcenik()!= null)
			ucenik = new UcenikDTO(fk.getUcenik());
		else 
			ucenik = new UcenikDTO();
	}

	public FinansijskaKarticaDTO(long id, String brojKartice, double suma, String ziroRacun, String pozivNaBroj,
			String brojModela, UcenikDTO ucenik) {
		super();
		this.id = id;
		this.brojKartice = brojKartice;
		this.suma = suma;
		this.ziroRacun = ziroRacun;
		this.pozivNaBroj = pozivNaBroj;
		this.brojModela = brojModela;
		this.ucenik = ucenik;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getSuma() {
		return suma;
	}

	public void setSuma(double suma) {
		this.suma = suma;
	}

	public UcenikDTO getUcenik() {
		return ucenik;
	}

	public void setUcenik(UcenikDTO ucenik) {
		this.ucenik = ucenik;
	}
	
	public String getBrojKartice() {
		return brojKartice;
	}

	public void setBrojKartice(String brojKartice) {
		this.brojKartice = brojKartice;
	}

	public String getZiroRacun() {
		return ziroRacun;
	}

	public void setZiroRacun(String ziroRacun) {
		this.ziroRacun = ziroRacun;
	}

	public String getPozivNaBroj() {
		return pozivNaBroj;
	}

	public void setPozivNaBroj(String pozivNaBroj) {
		this.pozivNaBroj = pozivNaBroj;
	}

	public String getBrojModela() {
		return brojModela;
	}

	public void setBrojModela(String brojModela) {
		this.brojModela = brojModela;
	}
	
	
	
	
}
