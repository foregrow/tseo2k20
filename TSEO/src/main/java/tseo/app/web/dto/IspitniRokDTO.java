package tseo.app.web.dto;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import tseo.app.entity.IspitniRok;
import tseo.app.enums.NazivRoka;

public class IspitniRokDTO {

	private long id;
	private NazivRoka nazivRoka;
	private Date pocetakRoka;
	private Date krajRoka;
	private List<IspitDTO> ispiti = new ArrayList<IspitDTO>();
	
	public IspitniRokDTO() {
		
	}
	
	public IspitniRokDTO(IspitniRok ir) {
		this.id = ir.getId();
		this.nazivRoka = ir.getNazivRoka();
		this.pocetakRoka = ir.getPocetakRoka();
		this.krajRoka = ir.getKrajRoka();
		
	}

	public IspitniRokDTO(long id, NazivRoka nazivRoka, Date pocetakRoka, Date krajRoka, List<IspitDTO> ispiti) {
		super();
		this.id = id;
		this.nazivRoka = nazivRoka;
		this.pocetakRoka = pocetakRoka;
		this.krajRoka = krajRoka;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public NazivRoka getNazivRoka() {
		return nazivRoka;
	}

	public void setNazivRoka(NazivRoka nazivRoka) {
		this.nazivRoka = nazivRoka;
	}

	public Date getPocetakRoka() {
		return pocetakRoka;
	}

	public void setPocetakRoka(Date pocetakRoka) {
		this.pocetakRoka = pocetakRoka;
	}

	public Date getKrajRoka() {
		return krajRoka;
	}

	public void setKrajRoka(Date krajRoka) {
		this.krajRoka = krajRoka;
	}

	public List<IspitDTO> getIspiti() {
		return ispiti;
	}

	public void setIspiti(List<IspitDTO> ispiti) {
		this.ispiti = ispiti;
	}

	
	
}
