package tseo.app.web.dto;



import java.util.List;
import java.util.Set;
import java.util.ArrayList;

import tseo.app.entity.Ispit;
import tseo.app.entity.Kolokvijum;
import tseo.app.entity.Predmet;
import tseo.app.entity.Ucenik;

public class PredmetDTO {

	private long id;
	private String naziv;
	//
	private int brojECTSBodova;
	private SmerDTO smer;
	private String datumPolaganja;
	private NastavnikDTO profesor;
	private List<UcenikDTO> ucenici = new ArrayList<UcenikDTO>();
	private List<KolokvijumDTO> kolokvijumi = new ArrayList<KolokvijumDTO>();
	private List<UcenikDTO> uceniciPrijaviliIspit = new ArrayList<UcenikDTO>();
	private List<IspitDTO> ispiti = new ArrayList<IspitDTO>();
	//
	public PredmetDTO() {
		
	}
	
	public PredmetDTO(Predmet predmet) {
		id = predmet.getId();
		naziv = predmet.getNaziv();
		brojECTSBodova = predmet.getBrojECTSBodova();
		datumPolaganja = predmet.getDatumPolaganja();
		if(predmet.getSmer() != null) 
			smer = new SmerDTO(predmet.getSmer());
		else 
			smer = new SmerDTO();
		
	}

	public PredmetDTO(long id, String naziv,int brojECTSBODOVA,SmerDTO smer,String datumPolaganja,List<UcenikDTO> ucenici,List<KolokvijumDTO> kolokvijumi,List<UcenikDTO> uceniciPrijaviliIspit,
			List<IspitDTO> ispiti) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.brojECTSBodova = brojECTSBODOVA;
		this.smer = smer;
		this.datumPolaganja = datumPolaganja;
		this.ucenici = ucenici;
		this.kolokvijumi = kolokvijumi;
		this.uceniciPrijaviliIspit = uceniciPrijaviliIspit;
		this.ispiti = ispiti;
	}
	
	public void setToList(Set<Ucenik> ucenici) {
		for(Ucenik u: ucenici)
			this.ucenici.add(new UcenikDTO(u));
	}
	public void setToListKlk(Set<Kolokvijum> klk) {
		for(Kolokvijum k: klk)
			this.kolokvijumi.add(new KolokvijumDTO(k));
	}
	public void ucenikToUcenikDTO(List<Ucenik> ucenici) {
		for(Ucenik u: ucenici)
			this.uceniciPrijaviliIspit.add(new UcenikDTO(u));
	}
	public void ispitiToDTO(List<Ispit> ispiti) {
		for(Ispit ispit: ispiti)
			this.ispiti.add(new IspitDTO(ispit));
	}
	//ispitiToDTO
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public int getBrojECTSBodova() {
		return brojECTSBodova;
	}

	public void setBrojECTSBodova(int brojECTSBodova) {
		this.brojECTSBodova = brojECTSBodova;
	}

	public SmerDTO getSmer() {
		return smer;
	}

	public void setSmer(SmerDTO smer) {
		this.smer = smer;
	}
	public String getDatumPolaganja() {
		return datumPolaganja;
	}

	public void setDatumPolaganja(String datumPolaganja) {
		this.datumPolaganja = datumPolaganja;
	}

	public NastavnikDTO getProfesor() {
		return profesor;
	}

	public void setProfesor(NastavnikDTO profesor) {
		this.profesor = profesor;
	}

	public List<UcenikDTO> getUcenici() {
		return ucenici;
	}

	public void setUcenici(List<UcenikDTO> ucenici) {
		this.ucenici = ucenici;
	}

	public List<KolokvijumDTO> getKolokvijumi() {
		return kolokvijumi;
	}

	public void setKolokvijumi(List<KolokvijumDTO> kolokvijumi) {
		this.kolokvijumi = kolokvijumi;
	}

	public List<UcenikDTO> getUceniciPrijaviliIspit() {
		return uceniciPrijaviliIspit;
	}

	public void setUceniciPrijaviliIspit(List<UcenikDTO> uceniciPrijaviliIspit) {
		this.uceniciPrijaviliIspit = uceniciPrijaviliIspit;
	}

	public List<IspitDTO> getIspiti() {
		return ispiti;
	}

	public void setIspiti(List<IspitDTO> ispiti) {
		this.ispiti = ispiti;
	}

}
