package tseo.app.web.dto;

import java.util.ArrayList;
import java.util.List;

import tseo.app.entity.Ucenik;
import tseo.app.enums.Pol;

public class UcenikDTO {

	private long id;
	private String ime;
	private String prezime;
	private String index;
	private int godinaUpisa;
	private int godinaStudija;
	private SmerDTO smer;
	
	//
	private String drzavaRodjenja;
	private String mestoRodjenja;
	private String datumRodjenja;
	private Pol pol;
	private String nacinFinansiranja;
	private String email;
	private String adresa;
	private int ukupnoECTSBodova;
	private double prosecnaOcena;
	private String redniBrojUpisa;
	
	private List<PredmetDTO> pohadja = new ArrayList<PredmetDTO>();
	private List<KolokvijumDTO> kolokvijumi = new ArrayList<KolokvijumDTO>();
	private List<SpisakUplataDTO> uplate = new ArrayList<SpisakUplataDTO>();
	private List<DokumentDTO> dokumenti = new ArrayList<DokumentDTO>();
	public UcenikDTO() {
		
	}
	
	public UcenikDTO(Ucenik uc) {
			id = uc.getId();
			ime = uc.getIme();
			prezime = uc.getPrezime();
			index = uc.getIndex();
			godinaUpisa = uc.getGodinaUpisa();
			godinaStudija = uc.getGodinaStudija();
			drzavaRodjenja = uc.getDrzavaRodjenja();
			mestoRodjenja = uc.getMestoRodjenja();
			datumRodjenja = uc.getDatumRodjenja();
			pol = uc.getPol();
			nacinFinansiranja = uc.getNacinFinansiranja();
			email = uc.getEmail();
			adresa = uc.getAdresa();
			ukupnoECTSBodova = uc.getUkupnoECTSBodova();
			prosecnaOcena = uc.getProsecnaOcena();
			redniBrojUpisa = uc.getRedniBrojUpisa();
			if(uc.getSmer()!= null) 
				smer = new SmerDTO(uc.getSmer());
			else
				smer = new SmerDTO();
		
	}


	public UcenikDTO(long id, String ime, String prezime, String index, int godinaUpisa, int godinaStudija,
			SmerDTO smer, String drzavaRodjenja, String mestoRodjenja, String datumRodjenja, Pol pol,
			String nacinFinansiranja, String email, String adresa, int ukupnoECTSBodova, double prosecnaOcena,
			String redniBrojUpisa, List<KolokvijumDTO> kolokvijumi,List<SpisakUplataDTO> uplate,List<DokumentDTO> dokumenti) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.index = index;
		this.godinaUpisa = godinaUpisa;
		this.godinaStudija = godinaStudija;
		this.smer = smer;
		this.drzavaRodjenja = drzavaRodjenja;
		this.mestoRodjenja = mestoRodjenja;
		this.datumRodjenja = datumRodjenja;
		this.pol = pol;
		this.nacinFinansiranja = nacinFinansiranja;
		this.email = email;
		this.adresa = adresa;
		this.ukupnoECTSBodova = ukupnoECTSBodova;
		this.prosecnaOcena = prosecnaOcena;
		this.redniBrojUpisa = redniBrojUpisa;
		this.kolokvijumi = kolokvijumi;
		this.uplate = uplate;
		this.dokumenti = dokumenti;
	}

	public List<DokumentDTO> getDokumenti() {
		return dokumenti;
	}

	public void setDokumenti(List<DokumentDTO> dokumenti) {
		this.dokumenti = dokumenti;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public int getGodinaUpisa() {
		return godinaUpisa;
	}

	public void setGodinaUpisa(int godinaUpisa) {
		this.godinaUpisa = godinaUpisa;
	}

	public int getGodinaStudija() {
		return godinaStudija;
	}

	public void setGodinaStudija(int godinaStudija) {
		this.godinaStudija = godinaStudija;
	}


	public SmerDTO getSmer() {
		return smer;
	}

	public void setSmer(SmerDTO smer) {
		this.smer = smer;
	}

	public String getDrzavaRodjenja() {
		return drzavaRodjenja;
	}

	public void setDrzavaRodjenja(String drzavaRodjenja) {
		this.drzavaRodjenja = drzavaRodjenja;
	}

	public String getMestoRodjenja() {
		return mestoRodjenja;
	}

	public void setMestoRodjenja(String mestoRodjenja) {
		this.mestoRodjenja = mestoRodjenja;
	}

	public String getDatumRodjenja() {
		return datumRodjenja;
	}

	public void setDatumRodjenja(String datumRodjenja) {
		this.datumRodjenja = datumRodjenja;
	}

	public Pol getPol() {
		return pol;
	}

	public void setPol(Pol pol) {
		this.pol = pol;
	}

	public String getNacinFinansiranja() {
		return nacinFinansiranja;
	}

	public void setNacinFinansiranja(String nacinFinansiranja) {
		this.nacinFinansiranja = nacinFinansiranja;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public int getUkupnoECTSBodova() {
		return ukupnoECTSBodova;
	}

	public void setUkupnoECTSBodova(int ukupnoECTSBodova) {
		this.ukupnoECTSBodova = ukupnoECTSBodova;
	}

	public double getProsecnaOcena() {
		return prosecnaOcena;
	}

	public void setProsecnaOcena(double prosecnaOcena) {
		this.prosecnaOcena = prosecnaOcena;
	}

	public String getRedniBrojUpisa() {
		return redniBrojUpisa;
	}

	public void setRedniBrojUpisa(String redniBrojUpisa) {
		this.redniBrojUpisa = redniBrojUpisa;
	}

	public List<PredmetDTO> getPohadja() {
		return pohadja;
	}

	public void setPohadja(List<PredmetDTO> pohadja) {
		this.pohadja = pohadja;
	}

	public List<KolokvijumDTO> getKolokvijumi() {
		return kolokvijumi;
	}

	public void setKolokvijumi(List<KolokvijumDTO> kolokvijumi) {
		this.kolokvijumi = kolokvijumi;
	}

	public List<SpisakUplataDTO> getUplate() {
		return uplate;
	}

	public void setUplate(List<SpisakUplataDTO> uplate) {
		this.uplate = uplate;
	}
	
	

	
}
