package tseo.app.web.dto;

import tseo.app.entity.Korisnik;
import tseo.app.enums.UlogaKorisnika;

public class KorisnikDTO {

	private long id;
	private String korisnickoIme;
	private String lozinka;
	private UlogaKorisnika uloga;
	
	private UcenikDTO ucenik;
	private NastavnikDTO nastavnik;
	
	
	public KorisnikDTO() {
		
	}
	
	public KorisnikDTO(Korisnik kor) {
			
			id = kor.getId();
			korisnickoIme = kor.getKorisnickoIme();
			lozinka = kor.getLozinka();
			uloga = kor.getUloga();
			if(kor.getNastavnik() != null) 
				nastavnik = new NastavnikDTO(kor.getNastavnik());
			else
				nastavnik = new NastavnikDTO();
			if(kor.getUcenik() != null) 
				ucenik = new UcenikDTO(kor.getUcenik());
			else
				ucenik = new UcenikDTO();
			
	}
	

	public KorisnikDTO(long id, String korisnickoIme, String lozinka, UlogaKorisnika uloga, UcenikDTO ucenik,
			NastavnikDTO nastavnik) {
		super();
		this.id = id;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.uloga = uloga;
		this.ucenik = ucenik;
		this.nastavnik = nastavnik;
	}


	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getKorisnickoIme() {
		return korisnickoIme;
	}



	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}



	public String getLozinka() {
		return lozinka;
	}



	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}



	


	public UlogaKorisnika getUloga() {
		return uloga;
	}


	public void setUloga(UlogaKorisnika uloga) {
		this.uloga = uloga;
	}


	public UcenikDTO getUcenik() {
		return ucenik;
	}



	public void setUcenik(UcenikDTO ucenik) {
		this.ucenik = ucenik;
	}



	public NastavnikDTO getNastavnik() {
		return nastavnik;
	}



	public void setNastavnik(NastavnikDTO nastavnik) {
		this.nastavnik = nastavnik;
	}


	
	
}
