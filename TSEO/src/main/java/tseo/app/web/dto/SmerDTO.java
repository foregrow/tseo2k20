package tseo.app.web.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import tseo.app.entity.Predmet;
import tseo.app.entity.Smer;

public class SmerDTO {

	private long id;
	private String naziv;
	private NastavnikDTO nastavnik;
	
	//dodato
	private int brojECTSBodova;
	private String oznakaSmera;
	//
	private List<PredmetDTO> predmeti = new ArrayList<PredmetDTO>();
	public SmerDTO() {
		
	}
	
	public SmerDTO(Smer smer) {
		id = smer.getId();
		naziv = smer.getNaziv();
		brojECTSBodova = smer.getBrojECTSBodova();
		oznakaSmera = smer.getOznakaSmera();
		if(smer.getNastavnik() != null) 
			nastavnik = new NastavnikDTO(smer.getNastavnik());
		else 
			nastavnik = new NastavnikDTO();
		
		
	}
	
	public SmerDTO(long id, String naziv, NastavnikDTO nastavnik,int brojECTSBodova,String oznakaSmera) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.nastavnik = nastavnik;
		this.brojECTSBodova = brojECTSBodova;
		this.oznakaSmera = oznakaSmera;
	}
	
	public void transformToPredmetiDTO(Set<Predmet> predmeti) {
		for(Predmet p : predmeti)
			this.predmeti.add(new PredmetDTO(p));
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public NastavnikDTO getNastavnik() {
		return nastavnik;
	}

	public void setNastavnik(NastavnikDTO nastavnik) {
		this.nastavnik = nastavnik;
	}

	public int getBrojECTSBodova() {
		return brojECTSBodova;
	}

	public void setBrojECTSBodova(int brojECTSBodova) {
		this.brojECTSBodova = brojECTSBodova;
	}

	public String getOznakaSmera() {
		return oznakaSmera;
	}

	public void setOznakaSmera(String oznakaSmera) {
		this.oznakaSmera = oznakaSmera;
	}

	public List<PredmetDTO> getPredmeti() {
		return predmeti;
	}

	public void setPredmeti(List<PredmetDTO> predmeti) {
		this.predmeti = predmeti;
	}
	
	
}
