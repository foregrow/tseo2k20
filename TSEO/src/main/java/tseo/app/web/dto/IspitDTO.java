package tseo.app.web.dto;



import java.util.ArrayList;
import java.util.List;

import tseo.app.entity.Ispit;
import tseo.app.enums.StatusIspita;


public class IspitDTO {

	private long id;
	private String datumPolaganja;
	private String datumPrijave;
	private int ocena;
	private int bodoviTeorija;
	private int bodoviVezbe;
	private boolean polozen =false; 
	private int cena;
	private StatusIspita status; 
	
	private PredmetDTO predmet;
	private UcenikDTO ucenik;
	private IspitniRokDTO ispitniRok;
	
	private List<Long> prijavljeniPredmeti = new ArrayList<Long>();
	private List<PredmetDTO> predmetiDatumPromena = new ArrayList<PredmetDTO>();
	private List<KolokvijumDTO> kolokvijumi = new ArrayList<KolokvijumDTO>();
	public IspitDTO() {
		
	}
	
	public IspitDTO(Ispit ispit) {
		id = ispit.getId();
		datumPolaganja = ispit.getDatumPolaganja();
		datumPrijave = ispit.getDatumPrijave();
		ocena = ispit.getOcena();
		bodoviTeorija = ispit.getBodoviTeorija();
		bodoviVezbe = ispit.getBodoviVezbe();
		polozen = ispit.isPolozen();
		cena = ispit.getCena();
		status = ispit.getStatus();
		if(ispit.getPredmet()!= null)
			predmet = new PredmetDTO(ispit.getPredmet());
		else
			predmet = new PredmetDTO();
		
		if(ispit.getUcenik()!= null)
			ucenik = new UcenikDTO(ispit.getUcenik());
		else
			ucenik = new UcenikDTO();
		
		if(ispit.getIspitniRok()!= null)
			ispitniRok = new IspitniRokDTO(ispit.getIspitniRok());
		else
			ispitniRok = new IspitniRokDTO();
		
		
		
	}

	public IspitDTO(long id, String datumPolaganja,String datumPrijave, int ocena, int bodoviTeorija,int bodoviVezbe, boolean polozen, int cena, PredmetDTO predmet, StatusIspita status,
			UcenikDTO ucenik, IspitniRokDTO ispitniRok, List<Long> prijavljeniPredmeti, List<KolokvijumDTO> kolokvijumi, List<PredmetDTO> predmetiDatumPromena) {
		super();
		this.id = id;
		this.datumPolaganja = datumPolaganja;
		this.ocena = ocena;
		this.bodoviTeorija = bodoviTeorija;
		this.bodoviVezbe = bodoviVezbe;
		this.polozen = polozen;
		this.cena = cena;
		this.predmet = predmet;
		this.ucenik = ucenik;
		this.ispitniRok = ispitniRok;
		this.datumPrijave = datumPrijave;
		this.prijavljeniPredmeti = prijavljeniPredmeti;
		this.kolokvijumi = kolokvijumi;
		this.status = status;
		this.predmetiDatumPromena =predmetiDatumPromena;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDatumPolaganja() {
		return datumPolaganja;
	}

	public void setDatumPolaganja(String datumPolaganja) {
		this.datumPolaganja = datumPolaganja;
	}
	

	public String getDatumPrijave() {
		return datumPrijave;
	}

	public void setDatumPrijave(String datumPrijave) {
		this.datumPrijave = datumPrijave;
	}

	public int getOcena() {
		return ocena;
	}

	public void setOcena(int ocena) {
		this.ocena = ocena;
	}


	public int getBodoviTeorija() {
		return bodoviTeorija;
	}

	public void setBodoviTeorija(int bodoviTeorija) {
		this.bodoviTeorija = bodoviTeorija;
	}

	public int getBodoviVezbe() {
		return bodoviVezbe;
	}

	public void setBodoviVezbe(int bodoviVezbe) {
		this.bodoviVezbe = bodoviVezbe;
	}

	public boolean isPolozen() {
		return polozen;
	}

	public void setPolozen(boolean polozen) {
		this.polozen = polozen;
	}

	public int getCena() {
		return cena;
	}

	public void setCena(int cena) {
		this.cena = cena;
	}

	public PredmetDTO getPredmet() {
		return predmet;
	}

	public void setPredmet(PredmetDTO predmet) {
		this.predmet = predmet;
	}


	public UcenikDTO getUcenik() {
		return ucenik;
	}

	public void setUcenik(UcenikDTO ucenik) {
		this.ucenik = ucenik;
	}

	public IspitniRokDTO getIspitniRok() {
		return ispitniRok;
	}

	public void setIspitniRok(IspitniRokDTO ispitniRok) {
		this.ispitniRok = ispitniRok;
	}

	public List<Long> getPrijavljeniPredmeti() {
		return prijavljeniPredmeti;
	}

	public void setPrijavljeniPredmeti(List<Long> prijavljeniPredmeti) {
		this.prijavljeniPredmeti = prijavljeniPredmeti;
	}

	public List<KolokvijumDTO> getKolokvijumi() {
		return kolokvijumi;
	}

	public void setKolokvijumi(List<KolokvijumDTO> kolokvijumi) {
		this.kolokvijumi = kolokvijumi;
	}

	public StatusIspita getStatus() {
		return status;
	}

	public void setStatus(StatusIspita status) {
		this.status = status;
	}

	public List<PredmetDTO> getPredmetiDatumPromena() {
		return predmetiDatumPromena;
	}

	public void setPredmetiDatumPromena(List<PredmetDTO> predmetiDatumPromena) {
		this.predmetiDatumPromena = predmetiDatumPromena;
	}
	
	
}
