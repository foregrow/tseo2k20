package tseo.app.enums;

public enum NazivRoka {

	JANUARSKI,
	FEBRUARSKI,
	MARTOVSKI,
	APRILSKI,
	JUNSKI,
	JULSKI,
	SEPTEMBARSKI,
	OKTOBARSKI
}
