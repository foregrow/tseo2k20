package tseo.app.enums;

public enum UlogaNastavnika {

	PROFESOR,
	ASISTENT,
	DEMONSTRATOR
}
