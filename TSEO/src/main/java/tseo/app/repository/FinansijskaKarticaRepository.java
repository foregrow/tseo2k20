package tseo.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import tseo.app.entity.FinansijskaKartica;

public interface FinansijskaKarticaRepository extends JpaRepository<FinansijskaKartica, Long> {

	FinansijskaKartica findByUcenikId(long id);
	
	@Query("select count(fk) from FinansijskaKartica fk")
	int countFinansijskeKartice();
	
	
	FinansijskaKartica findByBrojKartice(String brojKartice);
	
}
