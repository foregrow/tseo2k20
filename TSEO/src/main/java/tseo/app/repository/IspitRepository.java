package tseo.app.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import tseo.app.entity.Ispit;
import tseo.app.entity.Ucenik;
import tseo.app.enums.StatusIspita;

public interface IspitRepository extends JpaRepository<Ispit, Long> {

	@Query("select i from Ispit i where i.ucenik.id = :uid AND i.predmet.id = :pid AND i.ispitniRok.id = :irid AND i.status = 0")
	Ispit ispitZaOdjavu(long uid, long pid, long irid);
	
	List<Ispit> findByIspitniRokId(long irid);
	
	List<Ispit> findByUcenikIdAndStatus(long uid, StatusIspita status);
	
	@Query("select u from Ucenik u,Ispit isp where isp.predmet.id = :pid and isp.ispitniRok.id = :irid and isp.status = 0 and isp.polozen = 0 and u.id = isp.ucenik.id")
	List<Ucenik> getUceniciPrijaviliIspit(long pid,long irid);
	
	Ispit findByIspitniRokIdAndUcenikIdAndPredmetIdAndStatusAndPolozen(long irid, long uid, long pid, StatusIspita status, boolean polozen);
	
	List<Ispit> findByPredmetIdAndIspitniRokId(long pid, long irid);
}
