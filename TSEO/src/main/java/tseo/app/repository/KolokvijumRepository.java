package tseo.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import tseo.app.entity.Kolokvijum;

public interface KolokvijumRepository extends JpaRepository<Kolokvijum, Long> {

	
	@Query("select count(klk) from Kolokvijum klk where klk.ucenik.id = :uid and klk.predmet.id = :pid")
	int countBrojKlkova(long uid, long pid);
}
