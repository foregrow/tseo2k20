package tseo.app.repository;

import java.util.List;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import tseo.app.entity.Nastavnik;
public interface NastavnikRepository extends JpaRepository<Nastavnik, Long> {

	
	@Query("SELECT n FROM Nastavnik n where NOT EXISTS (select k.nastavnik.id from Korisnik k where n.id= k.nastavnik.id)") 
	List<Nastavnik> getAllFromNastavnik();

	@Query("SELECT n FROM Nastavnik n where NOT EXISTS (select s.nastavnik.id from Smer s where n.id= s.nastavnik.id)") 
	List<Nastavnik> getAllWhereSefKatedreNull();
	
	Nastavnik getByEmail(String email);
	
	//@Query("select predmet from Predmet predmet join predmet.nastavnici nastavnici where nastavnici.id = :id")
	//select n.* from nastavnik n, predaje p where p.predmet_id = 7 and n.uloga = 0 and n.id = p.nastavnik_id;
	@Query("select nastavnik from Nastavnik nastavnik join nastavnik.predmeti predmeti where predmeti.id = :predmetId and nastavnik.uloga = 0")
	Nastavnik getNastavnikByPredmet(long predmetId);
	
	Nastavnik getByKorisnikKorisnickoIme(String korIme);
	
	
}
