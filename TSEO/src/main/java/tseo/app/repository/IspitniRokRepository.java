package tseo.app.repository;



import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import tseo.app.entity.IspitniRok;

public interface IspitniRokRepository extends JpaRepository<IspitniRok, Long> {

	
	@Query("SELECT ir FROM IspitniRok ir WHERE (:trenutniDatum BETWEEN ir.pocetakRoka AND ir.krajRoka)")
	IspitniRok getTrenutniRok(Date trenutniDatum);
	
}
