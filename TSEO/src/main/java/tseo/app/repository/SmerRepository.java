package tseo.app.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import tseo.app.entity.Smer;

public interface SmerRepository extends JpaRepository<Smer, Long> {
	
	Smer findByNastavnikId(long id);
	
	Smer findByOznakaSmera(String oznakaSmera);
	
	@Query("select s from Smer s, Predmet p where p.smer.id = s.id and p.naziv = :predName")
	List<Smer> findSmeroviByPredmetNaziv(String predName);
}
