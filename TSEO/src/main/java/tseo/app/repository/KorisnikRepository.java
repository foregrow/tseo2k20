package tseo.app.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import tseo.app.entity.Korisnik;

public interface KorisnikRepository extends JpaRepository<Korisnik, Long> {

	Korisnik findByKorisnickoIme(String korIme);
	
	Korisnik findByNastavnikId(long id);
	
	Korisnik findByNastavnikIdAndKorisnickoIme(long idTrazenogNastavnika, String korisnickoImeUlogovanog);
	
	Korisnik findByUcenikIdAndKorisnickoIme(long idTrazenogUcenika, String korisnickoImeUlogovanog);
	
}
