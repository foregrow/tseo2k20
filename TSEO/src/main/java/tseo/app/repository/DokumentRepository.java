package tseo.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import tseo.app.entity.Dokument;

public interface DokumentRepository extends JpaRepository<Dokument, Long> {

	
	List<Dokument> findByUcenikId(long uid);
}
