package tseo.app.repository;

import java.util.List;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import tseo.app.entity.Predmet;

public interface PredmetRepository extends JpaRepository<Predmet, Long> {


	@Query("select predmet from Predmet predmet join predmet.nastavnici nastavnici where nastavnici.id = :id")
	List<Predmet> getPredmetiNastavnikPredaje(long id);
	
	@Query("select predmet from Predmet predmet join predmet.nastavnici nastavnici where nastavnici.uloga = 0")
	List<Predmet> getPredmetiNastavniciPredaju();
	
	@Query("select predmet from Predmet predmet where predmet.id not in "
			+ "(select pred.id from Predmet pred join pred.nastavnici nastavnici where nastavnici.id = :id)")
	List<Predmet> getPredmetiNastavnikNePredaje(long id);
	
	Predmet findByNaziv(String naziv);
	
	@Query("select predmet from Predmet predmet join predmet.ucenici ucenici where ucenici.id = :id")
	List<Predmet> getPredmetiUcenikPohadja(long id);
	
	//@Query("select p from Predmet p where p.id NOT IN (select sp.predmet_id from Predmet sp where sp.smer_id = :idSmera)")
	/*@Query("select p from Predmet p where p.id not in " + 
			"(select pred.id from Predmet pred join pred.smerovi smerovi where smerovi.id = :idSmera)")
	List<Predmet> getPredmetiNotInSmer(long idSmera);*/
	
	
	@Query("select p from Predmet p where p.smer.id = :smerId and p.id not in (select ip.predmet.id from Ispit ip where ip.ucenik.id = :ucenikId and ip.polozen = 1)")
	List<Predmet> getNepolozeniPredmeti(long smerId, long ucenikId);
	
	@Query("select p from Predmet p where p.smer.id = :smerId and p.id not in (select ip.predmet.id from Ispit ip where (ip.ucenik.id = :ucenikId and ip.polozen = 1) or (ip.ucenik.id = :ucenikId and ip.status = 0 and ip.ispitniRok.id = :iRok))")
	List<Predmet> getPredmetiZaPrijavu(long smerId, long ucenikId, long iRok);
	
	@Query("select p from Predmet p, Ispit ip where p.id = ip.predmet.id and ip.ucenik.id = :ucenikId and ip.status = 0 and ip.ispitniRok.id = :iRok ")
	List<Predmet> getPrijavljeniPredmetiZaIspit(long ucenikId,long iRok);
	
	
}
