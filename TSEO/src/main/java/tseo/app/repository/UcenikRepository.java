package tseo.app.repository;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import tseo.app.entity.Ucenik;

public interface UcenikRepository extends JpaRepository<Ucenik, Long> {
	
	@Query("SELECT u FROM Ucenik u where NOT EXISTS (select k.ucenik.id from Korisnik k where u.id= k.ucenik.id)") 
	List<Ucenik> getAllFromUcenik();
	
	Ucenik getByIndex(String index);
	
	@Query("select count(u) from Ucenik u where u.index like :str% and u.index like %:godinaUpisa")
	int countUceniciBySmerAndGodinaUpisa(String str,int godinaUpisa);
	
	@Query("select u.index from Ucenik u where u.index like :str% and u.index like %:godinaUpisa")
	List<String> getIndexesBySmerAndGodinaUpisa(String str,int godinaUpisa);
	

}
