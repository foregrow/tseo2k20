package tseo.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import tseo.app.entity.SpisakUplata;

public interface SpisakUplataRepository extends JpaRepository<SpisakUplata, Long> {

	
	List<SpisakUplata> findByUcenikId(long uid);
}
